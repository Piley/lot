$(function () { 
    $("[data-toggle='tooltip']").tooltip(); 
});

$( document ).ajaxStop( function() {
    $("[data-toggle='tooltip']").tooltip();
});


$( document ).ajaxStart( function() {
    $('#main-block').showLoading();
});

$( document ).ajaxStop( function() {
    $('#main-block').hideLoading();
});




// автосабмит при переключении шаблона поиска
$('body').on('change', '#lotsearch-template', function(){
    $('#lotsearch-form').submit();
});

// автосабмит при переключении шаблона поиска
$(document).on('click', '.pager-link', function(el){
    $('#pagination-input').val( $(this).data('page') )
    $('#lotsearch-form').submit();
    return false;
});

// экспорт лота в excel
$(document).on('click', '#lot-search-export', function(el){
    location.href = '/trade/export';
});

// создать шаблон поиска
function createTemplate(el) {
  $.ajax({
    type: "POST",
    url: "/trade/create-template",
    data: {name: $('#template-create-name').val()},
  });
}

// редактировать шаблон поиска
function editTemplate(el) {
  $.ajax({
    type: "POST",
    url: "/trade/edit-template",
    data: {name: $('#template-edit-name').val()},
  });
}

// удалить шаблон поиска
function deleteTemplate(el) {
  $.ajax({
    type: "POST",
    url: "/trade/delete-template",
  });
}

function addFavorite(el) {
  if ( $(el).hasClass('btn-success') ) {
    $(el).addClass('btn-default');
    $(el).removeClass('btn-success');
    $(el).empty();
    $(el).append("<i class='fa fa-minus'></i>");
  } else {
    $(el).addClass('btn-success');
    $(el).removeClass('btn-default');
    $(el).empty();
    $(el).append("<i class='fa fa-plus'></i>");
  }
  $.ajax({
    type: "POST",
    url: "/trade/favorite",
    data: {id: $(el).data('id')},
  });
}

function addTrash(el) {
  if ( $(el).hasClass('btn-danger') ) {
    $(el).addClass('btn-default');
    $(el).removeClass('btn-danger');
    $(el).empty();
    $(el).append("<i class='fa fa-reply'></i>");
  } else {
    $(el).addClass('btn-danger');
    $(el).removeClass('btn-default');
    $(el).empty();
    $(el).append("<i class='fa fa-trash'></i>");
  }
  $.ajax({
    type: "POST",
    url: "/trade/trash",
    data: {id: $(el).data('id')},
  });
}

function addMonitor(el) {
  if ( $(el).hasClass('btn-warning') ) {
    $(el).addClass('btn-default');
    $(el).removeClass('btn-warning');
    $(el).empty();
    $(el).append("<i class='fa fa-search-minus'></i>");
  } else {
    $(el).addClass('btn-warning');
    $(el).removeClass('btn-default');
    $(el).empty();
    $(el).append("<i class='fa fa-search-plus'></i>");
  }
  $.ajax({
    type: "POST",
    url: "/trade/monitor",
    data: {id: $(el).data('id')},
  });
}

// получить данные по заказу и отправить юзера на яндекс кассу
function paymentForm(el) {
  $.ajax({
    type: "POST",
    url: "/tariffs/get-order-data",
    data: {id: $(el).data('id')},
    dataType: 'json',
    success: function(jsondata){
      $('#payment-form-sum').val(jsondata.price);
      $('#payment-form-label').val(jsondata.order_id);
      // $('#payment-form-customerNumber').val(jsondata.user_id);
      // $('#payment-form').submit();
    }
  });
  $('#main-block').hideLoading();
}

function goRegister(el) {
  location.href = '/user/signup';
}

// все чекбоксы в модальном окне

// категории
function choiceAllCategories() {
  $('#lotsearch-categories input' ).each(function(i, elem) {
      $(this).prop('checked',true);
  });
}

function cancelAllCategories() {
  $('#lotsearch-categories input' ).each(function(i, elem) {
    $(this).prop('checked',false);
  });
}

// регионы
function choiceAllRegions() {
  $('#lotsearch-regions input' ).each(function(i, elem) {
      $(this).prop('checked',true);
  });
}

function cancelAllRegions() {
  $('#lotsearch-regions input' ).each(function(i, elem) {
    $(this).prop('checked',false);
  });
}

// площадки
function choiceAllPlatforms() {
  $('#lotsearch-platforms input' ).each(function(i, elem) {
      $(this).prop('checked',true);
  });
}

function cancelAllPlatforms() {
  $('#lotsearch-platforms input' ).each(function(i, elem) {
    $(this).prop('checked',false);
  });
}


// ежедневная рассылка
function activateNotice(el) {
  if ( $(el).hasClass('btn-warning') ) {
    $(el).addClass('btn-default');
    $(el).removeClass('btn-warning');
    $(el).empty();
    $(el).append("<i class='fa fa-check'></i> Отключить рассылку");
  } else {
    $(el).addClass('btn-warning');
    $(el).removeClass('btn-default');
    $(el).empty();
    $(el).append("<i class='fa fa-envelope-o'></i> Ежедневная рассылка");
  }
  $.ajax({
    type: "POST",
    url: "/calendar/notice",
  });
}