<?php

namespace app\models;

use Yii;


/**
 * This is the model class for table "{{%promo}}".
 *
 * @property integer $id
 * @property string $promo_key
 * @property integer $create_time
 * @property integer $use_time
 * @property integer $user_id
 * @property string $channel
 * @property string $extra_data
 */
class Promo extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%promo}}';
    }

    public function rules()
    {
        return [
            [['create_time', 'use_time', 'user_id'], 'integer'],
            [['promo_key'], 'string', 'max' => 16],
            [['channel'], 'string', 'max' => 128],
            [['extra_data'], 'string', 'max' => 512],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'promo_key' => 'Promo Key',
            'create_time' => 'Create Time',
            'use_time' => 'Use Time',
            'user_id' => 'User ID',
            'channel' => 'Channel',
            'extra_data' => 'Extra Data',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
