<?php

namespace app\models;

use Yii;

class BlogCategory extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%blog_category}}';
    }

    public function rules()
    {
        return [
            [['name', 'alias'], 'string', 'max' => 128],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'alias' => 'Alias',
        ];
    }

    public static function getForList()
    {
        $connection=Yii::$app->db;
        $sql = "select c.id id, c.name name, c.alias alias, count(*) cnt 
            FROM tb_blog b 
            left join tb_blog_category c
            on c.id = b.category_id
            group by c.id
            order by cnt desc";
        $list = $connection->createCommand($sql)->queryAll();;

        return $list;
    }

    public function getBlogs()
    {
        return $this->hasMany(Blog::className(), ['category_id' => 'id']);
    }
}
