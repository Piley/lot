<?php

namespace app\models;

use Yii;
use app\models\User;

class Payment extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%payment}}';
    }

    public function rules()
    {
        return [
            [['user_id', 'invoice_id', 'status_id', 'pay_time', 'success_time'], 'integer'],
            [['amount'], 'number'],
            [['description'], 'required'],
            [['description'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'invoice_id' => 'Invoice ID',
            'status_id' => 'Status ID',
            'pay_time' => 'Pay Time',
            'success_time' => 'Success Time',
            'amount' => 'Amount',
            'description' => 'Description',
        ];
    }

    // выдать платную подписку юзеру (применяется после paymentAviso яндекс-кассы)
    // $order = модель Payment
    public function giveAccess($order)
    {
        $tariff = Tariff::findOne($order->tariff_id);

        // проверить роль юзера, и установить новую, если она выше прежней 
        $user = User::findOne($order->user_id);
        if ($user->role < User::ROLE_CLIENT) {
            $user->role = User::ROLE_CLIENT;
        }
        $user->tariff_time = time() + $tariff->duration_time;
        $user->save(false);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getTariff()
    {
        return $this->hasOne(Tariff::className(), ['id' => 'tariff_id']);
    }
}
