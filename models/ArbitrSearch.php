<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Arbitr;

/**
 * ArbitrSearch represents the model behind the search form about `app\models\Arbitr`.
 */
class ArbitrSearch extends Arbitr
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'inn', 'reg_number', 'reg_time', 'begin_time'], 'integer'],
            [['name', 'contact_person', 'address', 'email', 'phone', 'fedresurs_link', 'sro_link'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Arbitr::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'inn' => $this->inn,
            'reg_number' => $this->reg_number,
            'reg_time' => $this->reg_time,
            'begin_time' => $this->begin_time,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'contact_person', $this->contact_person])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'fedresurs_link', $this->fedresurs_link])
            ->andFilterWhere(['like', 'sro_link', $this->sro_link]);

        return $dataProvider;
    }
}
