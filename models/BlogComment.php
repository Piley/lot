<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%blog_comment}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $blog_id
 * @property string $text
 * @property integer $create_time
 */
class BlogComment extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%blog_comment}}';
    }

    public function rules()
    {
        return [
            [['user_id', 'blog_id', 'create_time'], 'integer'],
            [['text'], 'required'],
            [['text'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'blog_id' => 'Blog ID',
            'text' => 'Text',
            'create_time' => 'Create Time',
        ];
    }

    public function getBlog()
    {
        return $this->hasOne(Blog::className(), ['id' => 'blog_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
