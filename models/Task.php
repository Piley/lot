<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_task".
 *
 * @property integer $id
 * @property integer $lot_id
 * @property integer $user_id
 * @property string $name
 * @property string $description
 * @property integer $create_time
 * @property integer $deadline_time
 *
 * @property Lot $lot
 * @property User $user
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lot_id', 'user_id', 'create_time', 'deadline_time'], 'integer'],
            [['description'], 'required'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 128],
            [['lot_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lot::className(), 'targetAttribute' => ['lot_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lot_id' => 'Lot ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'description' => 'Description',
            'create_time' => 'Create Time',
            'deadline_time' => 'Deadline Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLot()
    {
        return $this->hasOne(Lot::className(), ['id' => 'lot_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
