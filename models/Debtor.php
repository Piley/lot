<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%debtor}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property string $inn
 * @property string $ogrn 
 * @property string $phone 
 * @property string $category 
 * @property string $fedresurs_link
 *
 * @property Lot[] $lots
 */
class Debtor extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%debtor}}';
    }

    public function rules()
    {
        return [
            [['region_id', 'inn', 'kpp', 'ogrn', 'okpo', 'snils', 'birth_time', 'type'], 'integer'],
            [['short_name', 'full_name', 'address', 'name_history', 'add_info', 'fedresurs_link'], 'string', 'max' => 512],
            [['phone', 'category', 'okpf', 'birth_place'], 'string', 'max' => 128],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'short_name' => 'Наименование',
            'full_name' => 'Полное наименование',
            'address' => 'Адрес',
            'phone' => 'Телефон',
            'region_id' => 'Регио',
            'category' => 'Категория должника',
            'inn' => 'ИНН',
            'kpp' => 'КПП',
            'ogrn' => 'ОГРН',
            'okpo' => 'ОКПО',
            'okpf' => 'ОКПФ',
            'snils' => 'СНИЛС',
            'birth_time' => 'Дата рождения',
            'birth_place' => 'Место рождения',
            'name_history' => 'Ранее имевшиеся ФИО',
            'add_info' => 'Доп. информация',
            'type' => 'Тип(ООО/ИП)',
            'fedresurs_link' => 'Федресурс',
        ];
    }

    public function getLots()
    {
        return $this->hasMany(Lot::className(), ['debtor_id' => 'id']);
    }

    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }
}
