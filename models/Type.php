<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%type}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 *
 * @property Lot[] $lots
 * @property Lot[] $lots0
 */
class Type extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%type}}';
    }

    public function rules()
    {
        return [
            [['name', 'alias'], 'string', 'max' => 128],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'alias' => 'Alias',
        ];
    }

    public function getLots()
    {
        return $this->hasMany(Lot::className(), ['type_id' => 'id']);
    }
}
