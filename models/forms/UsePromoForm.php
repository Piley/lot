<?php
namespace app\models\forms;

use Yii;
use yii\base\Model;
use app\models\User;
use app\models\Promo;

class UsePromoForm extends Model
{
    public $promo_key;

    public function rules()
    {
        return [
            ['promo_key', 'trim'],
            ['promo_key', 'required'],
            // ['promo_key', 'string', 'max' => 16],
            ['promo_key', 'validatePromo'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'promo_key' => 'Промо-код',
        ];
    }

    // активация тарифа по промо-коду
    public function activatePromo()
    {
        $user = User::findOne(Yii::$app->user->identity->getId());
        $promo = Promo::find()
            ->where(['promo_key' => $this->promo_key])
            ->andWhere(['user_id' => 0])
            ->one();

        $promo->use_time = time();
        $promo->user_id = $user->id;
        $promo->save();

        // ставим юзеру 7 дней подписки
        $user->tariff_time = time() + 604800;
        $user->role = USER::ROLE_CLIENT;
        return $user->save();
    }

    public function validatePromo($promo_key, $params)
    {
        $model = Promo::find()
            ->where(['promo_key' => $this->promo_key])
            ->andWhere(['user_id' => 0])
            ->one();

        if (!$model) {
            $this->addError($promo_key, 'Вы ввели неверный промо-код');
        }

        if (User::isClient()) {
            $this->addError($promo_key, 'У вас уже есть платная подписка');
        }
    }
}
