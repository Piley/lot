<?php 
namespace app\models\forms;
 
use yii\web\UploadedFile;
use yii\base\Model;
use yii\db\ActiveQuery;
use Yii;

use app\models\User;

class ChangeAvatarForm extends Model
{
    public $avatar;
 
    /**
     * @var User
     */
    private $_user;
 
    public function __construct(User $user, $config = [])
    {
        $this->_user = $user;
        parent::__construct($config);
    }
 
    public function init()
    {
        $this->avatar = $this->_user->avatar;
        parent::init();
    }
 
    public function rules()
    {
        return [
            ['avatar', 'required'],
            ['avatar','image','extensions'=> ['jpg','jpeg','png']],
            // ['avatar', 'string', 'max' => 256],
        ];
    }
 
    public function upload()
    {
        if ($this->validate()) {
            $name = date('U') . $this->avatar->baseName . '.' . $this->avatar->extension;
            $this->avatar->saveAs('uploads/' . $name);
            $user = $this->_user;
            $user->avatar = $name;
            return $user->save();
        } else {
            return false;
        }
    }
}


