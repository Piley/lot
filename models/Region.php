<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%region}}".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $number
 * @property string $name
 * @property string $genitive
 *
 * @property Lot[] $lots
 */
class Region extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%region}}';
    }

    public function rules()
    {
        return [
            [['parent_id', 'number'], 'integer'],
            [['name', 'genitive', 'fedresurs_name'], 'string', 'max' => 128],        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'number' => 'Number',
            'name' => 'Name',
            'genitive' => 'Genitive',
            'fedresurs_name' => 'Fedresurs Name', 
        ];
    }

    public function getLots()
    {
        return $this->hasMany(Lot::className(), ['region_id' => 'id']);
    }
}
