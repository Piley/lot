<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%arbitr}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property string $inn
 * @property integer $reg_number 
 * @property integer $reg_time 
 * @property string $email
 * @property string $phone
 * @property string $fedresurs_link
 * 
 * @property Lot[] $lots 
 */
class Arbitr extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%arbitr}}';
    }

    public function rules()
    {
        return [
            [['inn', 'reg_number', 'reg_time'], 'integer'],
            [['name', 'address', 'email', 'fedresurs_link'], 'string', 'max' => 512],
            [['phone'], 'string', 'max' => 128],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'address' => 'Адрес',
            'inn' => 'ИНН',
            'reg_number' => 'Регистрационный номер', 
            'reg_time' => 'Дата регистрации', 
            'email' => 'Email',
            'phone' => 'Телефон',
            'fedresurs_link' => 'Ссылка на федресурс',
        ];
    }

    public function getLots()
    {
        return $this->hasMany(Lot::className(), ['arbitr_id' => 'id']);
    }
}
