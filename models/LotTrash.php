<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%lot_trash}}".
 *
 * @property integer $id
 * @property integer $lot_id
 * @property integer $user_id
 * @property integer $add_time
 *
 * @property Lot $lot
 * @property User $user
 */
class LotTrash extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%lot_trash}}';
    }

    public function rules()
    {
        return [
            [['lot_id', 'user_id', 'add_time'], 'integer'],
            [['lot_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lot::className(), 'targetAttribute' => ['lot_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lot_id' => 'Lot ID',
            'user_id' => 'User ID',
            'add_time' => 'Add Time',
        ];
    }

    public function getLot()
    {
        return $this->hasOne(Lot::className(), ['id' => 'lot_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
