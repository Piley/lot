<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%organizer}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $full_name 
 * @property string $contact_person
 * @property string $address
 * @property string $inn
 * @property string $kpp 
 * @property string $ogrn 
 * @property string $okpo 
 * @property string $okpf 
 * @property string $email
 * @property string $phone
 * @property string $fedresurs_link
 *
 * @property Lot[] $lots
 */
class Organizer extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%organizer}}';
    }

    public function rules()
    {
        return [
            [['region_id', 'inn', 'kpp', 'ogrn', 'okpo', 'type'], 'integer'],
            [['short_name', 'full_name', 'contact_person', 'address', 'email', 'fedresurs_link'], 'string', 'max' => 512],
            [['okpf', 'phone'], 'string', 'max' => 128],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'short_name' => 'Наименомание',
            'full_name' => 'Полное наименомание',
            'contact_person' => 'Контактное лицо',
            'region_id' => 'Регион',
            'address' => 'Адрес',
            'inn' => 'ИНН',
            'kpp' => 'КПП',
            'ogrn' => 'ОГРН',
            'okpo' => 'ОКПО',
            'okpf' => 'ОКПФ',
            'email' => 'Email',
            'phone' => 'Телефон',
            'type' => 'Тип (ИП/ООО)',
            'fedresurs_link' => 'Федресурс',
        ];
    }

    public function getLots()
    {
        return $this->hasMany(Lot::className(), ['organizer_id' => 'id']);
    }
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }
}
