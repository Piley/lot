<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Task;

class TaskSearch extends Task
{
    public function rules()
    {
        return [
            [['id', 'lot_id', 'user_id', 'create_time', 'deadline_time'], 'integer'],
            [['name', 'description'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    // вывод событий по лотам, которые юзер добавил в избранное
    public function search()
    {
        $connection=Yii::$app->db;
        $sql = "select id_lot id, name, type, time, color
            from
            (
            select l.id id_lot, l.description name, 'Начало подачи заявок' type, l.start_time time, '#006600' color from tb_lot l
            union
            select l.id id_lot, l.description name, 'Окончание подачи заявок' type, l.end_time time, '#993333' color from tb_lot l 
                where l.type_id = 1
            union
            select l.id id_lot, l.description name, 'Проведение торгов' type, l.start_time time, '#426cc8' color from tb_lot l
                where l.type_id = 1
            union
            select l.id id_lot, l.description name, CONCAT('Падение цены', ' ', i.price) type, i.start_time time, '#9966cc' color
                from tb_lot l join tb_lot_interval i on i.lot_id = l.id
            union
            select l.id id_lot, l.description name, t.name type, t.deadline_time time, '#336666' color
                from tb_lot l join tb_task t on t.lot_id = l.id
            ) tasks
            join tb_lot_monitor m 
            on tasks.id_lot = m.lot_id and m.user_id = ".Yii::$app->user->id."
            where time > 0
            order by time desc";
        $tasks = $connection->createCommand($sql)->queryAll();;

        return $tasks;
    }

    // вывод событий по лотам, которые юзер добавил в избранное
    public function searchForNotice($id)
    {
        // последние 2 дня 
        $before_time = time() - 172800;
        $connection=Yii::$app->db;
        $sql = "select id_lot id, name, type, time, color
            from
            (
            select l.id id_lot, l.description name, 'Начало подачи заявок' type, l.start_time time, '#006600' color from tb_lot l
            union
            select l.id id_lot, l.description name, 'Окончание подачи заявок' type, l.end_time time, '#993333' color from tb_lot l 
                where l.type_id = 1
            union
            select l.id id_lot, l.description name, 'Проведение торгов' type, l.start_time time, '#426cc8' color from tb_lot l
                where l.type_id = 1
            union
            select l.id id_lot, l.description name, CONCAT('Падение цены', ' ', i.price) type, i.start_time time, '#9966cc' color
                from tb_lot l join tb_lot_interval i on i.lot_id = l.id
            union
            select l.id id_lot, l.description name, t.name type, t.deadline_time time, '#336666' color
                from tb_lot l join tb_task t on t.lot_id = l.id
            ) tasks
            join tb_lot_monitor m 
            on tasks.id_lot = m.lot_id and m.user_id = ".$id."
            where time > ".$before_time."
            order by time desc";
        $tasks = $connection->createCommand($sql)->queryAll();;

        return $tasks;
    }
}
