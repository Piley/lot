<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%blog}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $text
 * @property integer $category_id
 * @property integer $user_id
 * @property integer $create_time
 * @property integer $update_time
 * @property integer $type
 * @property string $keywords
 * @property string $description
 * @property string $image
 * @property string $alias
 * @property integer $views
 */
class Blog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'keywords', 'description'], 'required'],
            [['text', 'keywords', 'description'], 'string'],
            [['category_id', 'user_id', 'create_time', 'update_time', 'type', 'views'], 'integer'],
            [['name', 'image', 'alias'], 'string', 'max' => 256],

            [['category_id', 'user_id', 'create_time', 'update_time', 'type', 'views'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'text' => 'Text',
            'category_id' => 'Category ID',
            'user_id' => 'User ID',
            'create_time' => 'Create Time',
            'update_time' => 'Update Time',
            'type' => 'Type',
            'keywords' => 'Keywords',
            'description' => 'Description',
            'image' => 'Image',
            'alias' => 'Alias',
            'views' => 'Views',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->create_time = time();
        } else {
            $this->update_time = time();
        }
        $this->user_id = Yii::$app->user->id;
        return parent::beforeSave($insert);
    }

    public function getCategory()
    {
        return $this->hasOne(BlogCategory::className(), ['id' => 'category_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getComments()
    {
        return $this->hasMany(Comments::className(), ['blog_id' => 'id']);
    }
}
