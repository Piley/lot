<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_lot_interval".
 *
 * @property integer $id
 * @property integer $lot_id
 * @property integer $number
 * @property integer $start_time
 * @property integer $end_time
 * @property string $price
 *
 * @property Lot $lot
 */
class LotInterval extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'tb_lot_interval';
    }

    public function rules()
    {
        return [
            [['lot_id', 'number', 'start_time', 'end_time'], 'integer'],
            [['price'], 'number'],
            [['lot_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lot::className(), 'targetAttribute' => ['lot_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lot_id' => 'Lot ID',
            'number' => 'Number',
            'start_time' => 'Start Time',
            'end_time' => 'End Time',
            'price' => 'Price',
        ];
    }

    public function getLot()
    {
        return $this->hasOne(Lot::className(), ['id' => 'lot_id']);
    }

    public static function convert($intervals, $now_price)
    {
        foreach ($intervals as &$interval) {
            // если это текущий интервал
            $interval['price'] = round($interval['price']);
            $interval['start_time'] = Yii::$app->formatter->asDatetime($interval['start_time'], "php: d.m.Y  h:i");
            
            if ($interval['price'] == $now_price) {
                $interval['price'] = [
                    'y' => $interval['price'], 
                    'marker' => [
                        'symbol'=> 'url(/img/asterisk.png)'
                    ],
                ];
            }
        }
        return $intervals;
    }
}
