<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%platform}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $link
 * @property string $owner
 * @property string $description
 *
 * @property Lot[] $lots
 */
class Platform extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%platform}}';
    }

    public function rules()
    {
        return [
            [['description'], 'required'],
            [['description'], 'string'],
            [['name', 'link', 'owner'], 'string', 'max' => 512],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'link' => 'Link',
            'owner' => 'Owner',
            'description' => 'Description',
        ];
    }

    public function getLots()
    {
        return $this->hasMany(Lot::className(), ['platform_id' => 'id']);
    }
}
