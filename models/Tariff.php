<?php

namespace app\models;

use Yii;

class Tariff extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%tariff}}';
    }

    public function rules()
    {
        return [
            [['price'], 'number'],
            [['duration_time'], 'integer'],
            [['description'], 'required'],
            [['description'], 'string'],
            [['name', 'type'], 'string', 'max' => 512],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'price' => 'Price',
            'duration_time' => 'Duration Time',
            'description' => 'Description',
        ];
    }
}
