<?php

namespace app\models;

use Yii;

class Lot extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%lot}}';
    }

    public function rules()
    {
        return [
            [['description', 'region_id', 'type_id', 'status_id', 'category_id', 'platform_id', 'debtor_id'], 'required'],
            [['description'], 'string'],
            [['region_id', 'type_id', 'status_id', 'category_id', 'platform_id', 'load_time', 'publish_time', 'start_time', 'end_time', 'auction_time', 'result_time', 'fedresurs_number', 'debtor_id', 'organizer_id', 'arbitr_id', 'deposit_percent',], 'integer'],
            [['start_price', 'now_price', 'market_price', 'deposit_price', 'auct_step'], 'number'],
            [['name', 'address', 'link', 'fedresurs_link', 'case_number'], 'string', 'max' => 512],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['region_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Type::className(), 'targetAttribute' => ['type_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['platform_id'], 'exist', 'skipOnError' => true, 'targetClass' => Platform::className(), 'targetAttribute' => ['platform_id' => 'id']],
            [['debtor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Debtor::className(), 'targetAttribute' => ['debtor_id' => 'id']],
            [['organizer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Organizer::className(), 'targetAttribute' => ['organizer_id' => 'id']],
            [['arbitr_id'], 'exist', 'skipOnError' => true, 'targetClass' => Arbitr::className(), 'targetAttribute' => ['arbitr_id' => 'id']],

            [['organizer_id', 'arbitr_id', 'debtor_id', 'start_price', 'now_price', 'market_price', 'deposit_price', 'auct_step', 'load_time', 'publish_time', 'start_time', 'end_time', 'auction_time', 'result_time', 'fedresurs_number', 'trade_id', 'deposit_percent',], 'default', 'value' => 0],

        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'Номер лота',
            'name' => 'Название',
            'description' => 'Описание',
            'address' => 'Адрес',
            'region_id' => 'Регион',
            'type_id' => 'Тип',
            'status_id' => 'Статус',
            'category_id' => 'Категория',
            'platform_id' => 'Площадка',
            'start_price' => 'Начальная цена',
            'now_price' => 'Текущая цена',
            'market_price' => 'Рыночная цена',
            'deposit_price' => 'Задаток',
            'deposit_percent' => 'Задаток (%)',
            'auct_step' => 'Шаг аукциона',
            'load_time' => 'Добавлено',
            'publish_time' => 'Опубликовано',
            'start_time' => 'Начало приема заявок',
            'end_time' => 'Окончание приема заявок',
            'auction_time' => 'Проведение торгов',
            'result_time' => 'Подведение итогов',
            'debtor_id' => 'Банкрот',
            'organizer_id' => 'Организатор торгов',
            'arbitr_id' => 'Арбитражный управляющий',
            'link' => 'Ссылка на торги',
            'fedresurs_link' => 'Федресурс',
            'fedresurs_number' => 'Номер сообщения на Федресурсе',
            'case_number' => 'Номер дела',

            'region.name' => 'Регион',
            'type.name' => 'Тип',
            'status.name' => 'Статус',
            'category.name' => 'Категория',
            'platform.name' => 'Площадка',
            'debtor.short_name' => 'Банкрот',
            'organizer.short_name' => 'Организатор торгов',

        ];
    }

    // добавление/удаление из избранного
    public function addFavorite()
    {
        $link = LotFavorite::findOne(['lot_id' => $this->id, 'user_id' => Yii::$app->user->id]);
        if (!$link) {
            $link = new LotFavorite();
            $link->lot_id = $this->id;
            $link->user_id = Yii::$app->user->id;
            $link->add_time = time();
            return $link->save();
        }
        $link->delete();
    }

    // добавление/удаление из мусора
    public function addTrash()
    {
        $link = LotTrash::findOne(['lot_id' => $this->id, 'user_id' => Yii::$app->user->id]);
        if (!$link) {
            $link = new LotTrash();
            $link->lot_id = $this->id;
            $link->user_id = Yii::$app->user->id;
            $link->add_time = time();
            return $link->save();
        }
        return $link->delete();
    }

    // добавление/удаление из отслеживаемого
    public function addMonitor()
    {
        $link = LotMonitor::findOne(['lot_id' => $this->id, 'user_id' => Yii::$app->user->id]);
        if (!$link) {
            $link = new LotMonitor();
            $link->lot_id = $this->id;
            $link->user_id = Yii::$app->user->id;
            $link->add_time = time();
            return $link->save();
        }
        return $link->delete();
    }

    public function getLotFavorite() 
    { 
        return $this->hasOne(LotFavorite::className(), ['lot_id' => 'id'])
            ->from(['fav' => LotFavorite::tableName()])
            ->onCondition('fav.user_id = '.Yii::$app->user->id);
    }

    public function getLotMonitor() 
    { 
        return $this->hasOne(LotMonitor::className(), ['lot_id' => 'id'])
            ->from(['mon' => LotMonitor::tableName()])
            ->onCondition('mon.user_id = '.Yii::$app->user->id);
    }

    public function getLotTrash() 
    { 
        return $this->hasOne(LotTrash::className(), ['lot_id' => 'id'])
            ->from(['tr' => LotTrash::tableName()])
            ->onCondition('tr.user_id = '.Yii::$app->user->id);
    } 

    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    public function getType()
    {
        return $this->hasOne(Type::className(), ['id' => 'type_id']);
    }

    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getPlatform()
    {
        return $this->hasOne(Platform::className(), ['id' => 'platform_id']);
    }

    public function getDebtor()
    {
        return $this->hasOne(Debtor::className(), ['id' => 'debtor_id']);
    }

    public function getOrganizer()
    {
        return $this->hasOne(Organizer::className(), ['id' => 'organizer_id']);
    }

    public function getArbitr()
    {
        return $this->hasOne(Arbitr::className(), ['id' => 'arbitr_id']);
    }
}
