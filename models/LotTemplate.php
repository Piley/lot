<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%lot_template}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $data
 * @property integer $status
 */
class LotTemplate extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%lot_template}}';
    }

    public function rules()
    {
        return [
            [['user_id', 'status'], 'integer'],
            [['data'], 'string'],
            [['name'], 'string', 'max' => 128],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'data' => 'Data',
            'status' => 'Status',
        ];
    }

    public static function getForList($userid)
    {
        return \yii\helpers\ArrayHelper::map(
            static::find()
                ->select('id, name')
                ->where('user_id = '.$userid)
                ->all(),
            'id', 
            'name'
        );
    }

    public static function switchTemplate($id)
    {
        $temp = static::find()
            ->where(['user_id' => Yii::$app->user->id, 'status' => 1])
            ->one();

        if ($temp) {
            $temp->status = 0;
            $temp->save(false);

            $newtemp = static::findOne($id);
            $newtemp->status = 1;
            $newtemp->save(false);

        // на случай, если дефолтный шаблон удалился
        } else {
            $newtemp = new LotTemplate();
            $newtemp->name = 'Шаблон 1';
            $newtemp->user_id = Yii::$app->user->id;
            $newtemp->status = 1;
            $newtemp->data = serialize('');

            $newtemp->save(false);
        }
        

        return $newtemp;
    }
}
