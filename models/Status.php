<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%status}}".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Lot[] $lots
 */
class Status extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%status}}';
    }

    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 128],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    public function getLots()
    {
        return $this->hasMany(Lot::className(), ['status_id' => 'id']);
    }
}
