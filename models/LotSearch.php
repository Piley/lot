<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Lot;
use app\models\LotTemplate;

class LotSearch extends Lot
{
    public $fullsearch;
    public $categories;
    public $regions;
    public $platforms;
    public $types;
    public $statuses;
    public $from_date;
    public $to_date;
    public $from_price;
    public $to_price;
    public $template;
    public $sort;

    public function rules()
    {
        return [
            [['id', 'region_id', 'type_id', 'status_id', 'category_id', 'platform_id', 'publish_time', 'start_time', 'end_time', 'result_time', 'debtor_id', 'organizer_id', 'arbitr_id', 'template'], 'integer'],
            [['sort', 'fullsearch', 'template', 'regions', 'platforms', 'types', 'statuses', 'categories', 'from_date', 'to_date', 'from_price', 'to_price', 'name', 'description', 'address', 'link', 'fedresurs_link', 'case_number'], 'safe'],
            [['from_price', 'to_price', 'start_price', 'now_price', 'market_price', 'deposit_price', 'auct_step'], 'number'],
            [['sort'], 'default', 'value' => 'load_time'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'fullsearch' => 'Поиск по торгам',
            'categories' => 'Выбор категории',
            'regions' => 'Выбор региона',
            'platforms' => 'Выбор площадки',
            'types' => 'Тип торгов',
            'statuses' => 'Статус',
            'from_date' => 'Дата начала',
            'to_date' => 'Дата окончания',
            'from_price' => 'От',
            'to_price' => 'До',
            'template' => 'Шаблон поиска',
            'sort' => 'Сортировка',
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    // выборка для страницы торгов
    public function search($params)
    {
        // выборка со всеми join
        $query = $this->findListQuery();

        // прогрузка для избавления от _csrf
        $this->load($params);

        // проверка какой шаблон грузить
        if (!Yii::$app->user->isGuest) {

            // находим дефолтный шаблон пользователя
            $temp = LotTemplate::find()
                ->where(['user_id' => Yii::$app->user->id, 'status' => 1])
                ->one();

            // если get, значит грузим дефолтный шаблон из бд
            if (Yii::$app->request->isGet) {

                $this->load(unserialize($temp->data));
                
            // если post/ajax
            } else {
                // если шаблон отличается от дефолтного
                // меняем шаблон на нужный
                if ($this->template != $temp->id) {
                    $newtemp = LotTemplate::switchTemplate($this->template);
                    $this->load(unserialize($newtemp->data));
                
                // если шаблон тот же, то обновляем шаблон
                } else {
                    $temp->data = serialize($params);
                    $temp->save();
                }
            }

            // не показывать лоты из мусора
            $query->andWhere('ISNULL(tr.id)');
        }


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $query;
        }


        // фильтры

        // строка поиска
        if (trim($this->fullsearch)) {
            $query->where("l.id like '%$this->fullsearch%'
                or l.name like '%$this->fullsearch%' 
                or l.description like '%$this->fullsearch%'
                or l.address like '%$this->fullsearch%'
                or c.name like '%$this->fullsearch%'
                or r.name like '%$this->fullsearch%'
                or d.short_name like '%$this->fullsearch%'
                or d.inn like '%$this->fullsearch%'
                or o.short_name like '%$this->fullsearch%'
                or o.inn like '%$this->fullsearch%'
            ");
        }

        // чекбоксы типов
        $query->andFilterWhere(['in', 't.id', $this->types]);

        // чекбоксы категорий
        $query->andFilterWhere(['in', 'c.id', $this->categories]);

        // чекбоксы регионов
        $query->andFilterWhere(['in', 'r.id', $this->regions]);

        // чекбоксы площадок
        $query->andFilterWhere(['in', 'p.id', $this->platforms]);

        // чекбоксы статусов
        $query->andFilterWhere(['in', 's.id', $this->statuses]);

        // диапазон цен
        $query->andFilterWhere(['>=', 'now_price', $this->from_price]);
        $query->andFilterWhere(['<=', 'now_price', $this->to_price]);

        $this->start_time = $this->from_date ? strtotime($this->from_date) : null;
        $this->end_time = $this->to_date ? strtotime($this->to_date) : null;

        // диапазон дат
        $query->andFilterWhere(['>=', 'start_time', $this->start_time]);
        $query->andFilterWhere(['<=', 'end_time', $this->end_time]);

        $this->sort = $this->checkSortField() ? $this->sort : 'load_time';
        $query->orderBy([$this->sort => SORT_DESC]);
        
        return $query;
    }

    // выборка для экспорта в excel
    public function searchForExport()
    {
        // выборка со всеми join
        $query = $this->findListQuery();

        // находим дефолтный шаблон пользователя
        $temp = LotTemplate::find()
            ->where(['user_id' => Yii::$app->user->id, 'status' => 1])
            ->one();

        $this->load(unserialize($temp->data));

        // строка поиска
        if (trim($this->fullsearch)) {
            $query->where("l.id like '%$this->fullsearch%'
                or l.name like '%$this->fullsearch%' 
                or l.description like '%$this->fullsearch%'
                or l.address like '%$this->fullsearch%'
                or c.name like '%$this->fullsearch%'
                or r.name like '%$this->fullsearch%'
                or d.name like '%$this->fullsearch%'
                or d.inn like '%$this->fullsearch%'
                or o.name like '%$this->fullsearch%'
                or o.inn like '%$this->fullsearch%'
            ");
        }
        
        // чекбоксы типов
        $query->andFilterWhere(['in', 't.id', $this->types]);

        // чекбоксы категорий
        $query->andFilterWhere(['in', 'c.id', $this->categories]);

        // чекбоксы регионов
        $query->andFilterWhere(['in', 'r.id', $this->regions]);

        // чекбоксы площадок
        $query->andFilterWhere(['in', 'p.id', $this->platforms]);

        // чекбоксы статусов
        $query->andFilterWhere(['in', 's.id', $this->statuses]);

        // диапазон цен
        $query->andFilterWhere(['>=', 'now_price', $this->from_price]);
        $query->andFilterWhere(['<=', 'now_price', $this->to_price]);

        $this->start_time = $this->from_date ? strtotime($this->from_date) : null;
        $this->end_time = $this->to_date ? strtotime($this->to_date) : null;

        // диапазон дат
        $query->andFilterWhere(['>=', 'start_time', $this->start_time]);
        $query->andFilterWhere(['<=', 'end_time', $this->end_time]);

        $this->sort = $this->checkSortField() ? $this->sort : 'load_time';
        $query->orderBy([$this->sort => SORT_DESC]);
        
        return $query;
    }

    // общий запрос на выборку
    public function findListQuery()
    {
        // выборка со всеми join
        $query = Lot::find()->from('{{%lot}} l')
        ->joinWith('region r')
        ->joinWith('type t')
        ->joinWith('status s')
        ->joinWith('category c')
        ->joinWith('platform p')
        ->joinWith('debtor d')
        ->joinWith('organizer o')
        ->joinWith('arbitr ar');

        // если юзер авторизован, то join избранного, мусора, отслеживаемого
        if (!Yii::$app->user->isGuest) {
            $query->joinWith('lotFavorite fav',  true, 'LEFT JOIN')
            ->joinWith('lotTrash tr',  true, 'LEFT JOIN')
            ->joinWith('lotMonitor mon',  true, 'LEFT JOIN');
        }

        return $query;
    }

    // выборка избранного
    public function findFavoriteList()
    {
        $query = $this->findListQuery();

        $query->andWhere('fav.id is not null')
        ->orderBy(['fav.add_time'=>SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }

    // выборка мусора
    public function findTrashList()
    {
        $query = $this->findListQuery();
        
        $query->andWhere('tr.id is not null')
        ->orderBy(['tr.add_time'=>SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }

    // выборка отслеживаемого
    public function findMonitorList()
    {
        $query = $this->findListQuery();
        
        $query->andWhere('mon.id is not null')
        ->orderBy(['mon.add_time'=>SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }

    // валидация поля сортировки
    public function checkSortField()
    {
        $arr = [
            'load_time',
            'start_time',
            'end_time',
            'start_price',
            'now_price',
        ];
        if ( in_array($this->sort, $arr) ) return true;
        return false;
    }

}
