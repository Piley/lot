<?php
namespace app\commands;
 
use Yii;
use yii\console\Controller;
use \app\rbac\UserRoleRule;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll(); //удаляем старые данные
        //Создадим для примера права для доступа к админке
        $admin_panel = $auth->createPermission('admin_panel');
        $admin_panel->description = 'Админ панель';
        $auth->add($admin_panel);

        $basic_access = $auth->createPermission('basic_access');
        $basic_access->description = 'Обычная подписка';
        $auth->add($basic_access);

        //Включаем наш обработчик
        $rule = new UserRoleRule();
        $auth->add($rule);
        //Добавляем роли
        $user = $auth->createRole('user');
        $user->description = 'Пользователь';
        $user->ruleName = $rule->name;
        $auth->add($user);

        $client = $auth->createRole('client');
        $client->description = 'Клиент';
        $client->ruleName = $rule->name;
        $auth->add($client);

        $moder = $auth->createRole('moder');
        $moder->description = 'Модератор';
        $moder->ruleName = $rule->name;
        $auth->add($moder);

        //Добавляем потомков

        $auth->addChild($client, $user);
        $auth->addChild($client, $basic_access);

        $auth->addChild($moder, $client);
        $auth->addChild($moder, $basic_access);
        $auth->addChild($moder, $admin_panel);

        $admin = $auth->createRole('admin');
        $admin->description = 'Администратор';
        $admin->ruleName = $rule->name;
        $auth->add($admin);
        $auth->addChild($admin, $moder);
    }
}
