<?php

namespace app\commands;

use yii\console\Controller;
use app\models\User;
use app\models\Task;
use yii\helpers\StringHelper;

class NoticeController extends Controller
{
    // рассылка уведомлений об отслеживаемых торгах
    public function actionSend()
    {
        $notice_email = Yii::$app->params['notice']['email'];
        $notice_name = Yii::$app->params['notice']['name'];
        $notice_subject = Yii::$app->params['notice']['subject'];

        $users = User::find()
                ->where(['notice' => 1])
                ->andWhere('>=', 'role', User::ROLE_CLIENT)
                ->all();

        foreach ($users as $user) {         
            $searchModel = new TaskSearch();
            $tasks = $searchModel->searchForNotice($user->id);
            $body = self::formatEmail($tasks);
            Yii::$app->mailer->compose()
                ->setTo($user->email)
                ->setFrom([$notice_email => $notice_name])
                ->setSubject($notice_subject)
                ->setTextBody($body)
                ->send();
        }
    }

    private static function formatEmail($tasks)
    {
        $msg = '';
        foreach ($tasks as $task) {
            $item = '';
            $task['name'] = str_replace("<br>"," ",$task['name']);
            $item .= StringHelper::truncate($task['name'], 50, '...').' | '.$task['type'].'<br>';
            $item .= ' | ';
            $item .= $task['type'];
            $item .= ' | ';
            $item .= date('Y-m-d\TH:i:s\Z', $task['time']);
            $item .= '<br>';
            $msg .= $item;
        }
        return $msg;
    }
}
