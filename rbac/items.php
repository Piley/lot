<?php
return [
    'admin_panel' => [
        'type' => 2,
        'description' => 'Админ панель',
    ],
    'basic_access' => [
        'type' => 2,
        'description' => 'Обычная подписка',
    ],
    'user' => [
        'type' => 1,
        'description' => 'Пользователь',
        'ruleName' => 'userRole',
    ],
    'client' => [
        'type' => 1,
        'description' => 'Клиент',
        'ruleName' => 'userRole',
        'children' => [
            'user',
            'basic_access',
        ],
    ],
    'moder' => [
        'type' => 1,
        'description' => 'Модератор',
        'ruleName' => 'userRole',
        'children' => [
            'client',
            'basic_access',
            'admin_panel',
        ],
    ],
    'admin' => [
        'type' => 1,
        'description' => 'Администратор',
        'ruleName' => 'userRole',
        'children' => [
            'moder',
        ],
    ],
];
