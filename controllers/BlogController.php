<?php

namespace app\controllers;

use Yii;
use app\models\Blog;
use app\models\BlogCategory;
use app\models\BlogComment;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use yii\data\Pagination;

class BlogController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'delete'],
                'rules' => [
                    // разрешаем модераторам и выше
                    [
                        'allow' => true,
                        'roles' => ['moder'],
                    ],
                ],
            ],  
        ];
    }

    public function actionIndex()
    {
        $query = Blog::find()->andFilterWhere(['category_id' => Yii::$app->request->get('category')]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);
        $pages->pageSizeParam = false;
        $items = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'models' => $items,
            'pages' => $pages,
            'cats' => BlogCategory::getForList(),
            'total' => Blog::find()->count(),
        ]);
    }

    public function actionView($id)
    {
        $newcomment = new BlogComment();

        if (Yii::$app->request->isPost) {
            if (!Yii::$app->user->isGuest) {
                $newcomment->user_id = Yii::$app->user->id;
                $newcomment->blog_id = $id;
                $newcomment->create_time = time();
                $newcomment->text = Yii::$app->request->post('text');

                if ($newcomment->validate()) {
                    $newcomment->save();
                }
            }
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
            'comments' => BlogComment::find()
                ->where(['blog_id' => $id])
                ->orderBy(['create_time' => SORT_DESC])
                ->all(),
            'cats' => BlogCategory::getForList(),
            'total' => Blog::find()->count(),
            'newcomment' => $newcomment,
        ]);
    }

    public function actionCreate()
    {
        $model = new Blog();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Blog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
