<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\Lot;
use app\models\LotSearch;
use app\models\LotInterval;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use app\components\CustomPagination;

class TradeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'delete'],
                'rules' => [
                    // разрешаем модераторам и выше
                    [
                        'allow' => true,
                        'roles' => ['moder'],
                    ],
                ],
            ],        
        ];
    }

    // список лотов с фильтром
    public function actionIndex()
    {
        $searchModel = new LotSearch();
        $query = $searchModel->search(Yii::$app->request->post());

        $countQuery = clone $query;
        $pages = new CustomPagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);
        $pages->pageSizeParam = false;
        $lots = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();


        // $lots = $query->all();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'lots' => $lots,
            'pages' => $pages,
            'total' => $countQuery->count(),
        ]);

    }

    // просмотр отдельного лота
    public function actionView($id)
    {
        $lot = $this->findModelForView($id);

        $intervals = LotInterval::find()
            ->select(['start_time', 'price'])
            ->where(['lot_id' => $id])
            ->orderBy('start_time')
            ->asArray()
            ->all();
        $intervals = LotInterval::convert($intervals, $lot->now_price);


        return $this->render( User::isClient() ? 'view' : 'view-guest', 
        [
            'model' => $lot,
            'intervals' => $intervals,
        ]);
    }

    // добавить лот в избранное
    public function actionFavorite()
    {
        $id = Yii::$app->request->post('id');
        $this->findModel($id)->addFavorite();
        return $id;
    }

    // добавить лот в мусор
    public function actionTrash()
    {
        $id = Yii::$app->request->post('id');
        $this->findModel($id)->addTrash();
        return $id;
    }

    // следить за лотом
    public function actionMonitor()
    {
        $id = Yii::$app->request->post('id');
        $this->findModel($id)->addMonitor();
    }

    // создать новый лот
    public function actionCreate()
    {
        $model = new Lot();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    // редактировать лот
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    // удалить лот
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    // создать шаблон поиска
    public function actionCreateTemplate()
    {
        if (Yii::$app->request->isAjax) {
            if (User::isClient()) {
                $temp = new \app\models\LotTemplate;

                $name = Yii::$app->request->post('name');
                $temp->name = $name ? $name : 'Новый шаблон';
                $temp->user_id = Yii::$app->user->id;
                $temp->save(false);
            }
        }
        return $this->redirect(['index']);
    }

    // редактировать шаблон поиска
    public function actionEditTemplate()
    {
        if (Yii::$app->request->isAjax) {
            if (User::isClient()) {
                $temp = \app\models\LotTemplate::find()
                    ->where(['user_id' => Yii::$app->user->id, 'status' => 1])
                    ->one();

                $name = Yii::$app->request->post('name');
                $temp->name = $name ? $name : 'Новый шаблон';
                $temp->save(false);
            }
        }
        return $this->redirect(['index']);
    }

    // удалить шаблон поиска
    public function actionDeleteTemplate()
    {
        if (Yii::$app->request->isAjax) {
            if (User::isClient()) {
                $query = \app\models\LotTemplate::find()
                    ->where(['user_id' => Yii::$app->user->id]);

                // если у пользователя больше 1 шаблона
                if ($query->count() > 1) {
                    // удаляем активный
                    $temp = \app\models\LotTemplate::find()
                        ->where(['user_id' => Yii::$app->user->id, 'status' => 1])
                        ->one();
                    $temp->delete();

                    // делаем активным другой шаблон
                    $totemp = \app\models\LotTemplate::find()
                        ->where(['user_id' => Yii::$app->user->id])->one();
                    $totemp->status = 1;
                    $totemp->save(false);
                } 
            }
        }
        return $this->redirect(['index']);
    }

    // экспорт в excel
    public function actionExport()
    {
        if (!Yii::$app->user->isGuest) {

            $searchModel = new LotSearch();
            $lots = $searchModel->searchForExport()
                ->limit(100)
                ->all();

            $file = \moonland\phpexcel\Excel::widget([
                'models' => $lots,
                'mode' => 'export',
                'columns' => [
                    'id',
                    'name',
                    'description',
                    'region.name',
                    'type.name',
                    'status.name',
                    'category.name',
                    'platform.name',
                    'start_price',
                    'now_price',
                    'deposit_price',
                    'auct_step',
                    'market_price',
                    'publish_time:date',
                    'start_time:date',
                    'end_time:date',
                    'auction_time:date',
                    'result_time:date',
                    'debtor.name',
                    'organizer.name',
                    'link',
                    'fedresurs_link',
                ], 
            ]);
        }
        return $this->redirect(['index']);
    }

    // поиск лота по id
    protected function findModel($id)
    {
        if (($model = Lot::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelForView($id)
    {
        $model = Lot::find()
            ->from('{{%lot}} l')
            ->andWhere(['l.id' => $id]);

        if (!Yii::$app->user->isGuest) {
            $model->joinWith('lotFavorite fav',  true, 'LEFT JOIN')
                ->joinWith('lotTrash tr',  true, 'LEFT JOIN')
                ->joinWith('lotMonitor mon',  true, 'LEFT JOIN');
        }

        $model = $model->one();

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
