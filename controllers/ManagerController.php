<?php

namespace app\controllers;

use Yii;
use app\models\Lot;
use app\models\LotSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class ManagerController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    // только для зарегистрированных
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ], 
        ];
    }

    public function actionIndex()
    {
        return $this->redirect(['favorite']);
    }
    
    public function actionFavorite()
    {
        $searchModel = new LotSearch();
        $dataProvider = $searchModel->findFavoriteList();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'tab' => '_lot_item_favorite',
        ]);
    }

    public function actionTrash()
    {
        $searchModel = new LotSearch();
        $dataProvider = $searchModel->findTrashList();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'tab' => '_lot_item_trash',
        ]);
    }

    public function actionMonitor()
    {
        $searchModel = new LotSearch();
        $dataProvider = $searchModel->findMonitorList();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'tab' => '_lot_item_monitor',
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Lot::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
