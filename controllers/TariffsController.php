<?php

namespace app\controllers;

use Yii;
use app\models\Tariff;
use app\models\Payment;
use yii\web\Controller;

class TariffsController extends Controller
{
    public function behaviors()
    {
        return [];
    }

    public function actionIndex()
    {
        $tariffs = Tariff::find()->all();

        return $this->render('index', [
            'tariffs' => $tariffs,
        ]);
    }

    // создание платежа после нажатия на кнопку "оплатить"
    public function actionGetOrderData()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $data = [];

            if (!Yii::$app->user->isGuest) {

                $tariff_id = Yii::$app->request->post('id');
                $tariff_model = Tariff::findOne($tariff_id);

                if ($tariff_model) {
                    $pay = new Payment();
                    $pay->tariff_id = $tariff_model->id;
                    $pay->amount = $tariff_model->price;
                    $pay->description = $tariff_model->name;
                    $pay->user_id = Yii::$app->user->id;
                    $pay->pay_time = time();
                    $pay->status_id = 0;
                    if ( $pay->save(false) ) {
                        $data['price'] = $pay->amount;
                        $data['user_id'] = $pay->user_id;
                        $data['order_id'] = $pay->id;
                    }
                }
            }

            return $data;
        }

        return $this->redirect(['index']);
    }
}
