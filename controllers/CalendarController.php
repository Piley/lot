<?php

namespace app\controllers;

use Yii;
use app\models\Task;
use app\models\TaskSearch;
use yii\helpers\Url;
use yii\helpers\StringHelper;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class CalendarController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    // только для зарегистрированных
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],  
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionJsoncalendar($start=NULL,$end=NULL,$_=NULL){

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $searchModel = new TaskSearch();
        $tasks = $searchModel->search();

        $events = [];

        foreach ($tasks AS $task){
          $Event = new \yii2fullcalendar\models\Event();
          // $Event->id = $task['id'];

          $task['name'] = str_replace("<br>"," ",$task['name']);
          $Event->title = StringHelper::truncate($task['name'], 50, '...').' | '.$task['type'];
          $Event->color = $task['color'];
          $Event->url = Url::to(['trade/view', 'id' => $task['id']]);
          $Event->start = date('Y-m-d\TH:i:s\Z', $task['time']);
          $Event->end = date('Y-m-d\TH:i:s\Z', $task['time']);
          $events[] = $Event;
        }

        return $events;
    }

    // кнопка включить уведомления
    public function actionNotice()
    {
        if (!Yii::$app->user->isGuest) {
            $user = \app\models\User::findOne(Yii::$app->user->id);
            if (\app\models\User::isClient()){
                $user->notice = $user->notice ? 0 : 1;
                $user->save(false);
            }
        }
    }

    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
