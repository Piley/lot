<?php

namespace app\controllers;

use Yii;
use app\models\Arbitr;
use app\models\ArbitrSearch;
use app\models\Lot;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class ArbitrController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'delete'],
                'rules' => [
                    // разрешаем модераторам и выше
                    [
                        'allow' => true,
                        'roles' => ['moder'],
                    ],
                ],
            ],  
        ];
    }

    public function actionIndex()
    {
        $searchModel = new ArbitrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render(\app\models\User::isClient() ? 'view' : 'view-guest', [
            'model' => $this->findModel($id),
            'lots' => Lot::find()
                ->where(['arbitr_id' => $id])
                ->limit(10)
                ->orderBy(['id' => SORT_DESC])
                ->all(),
        ]);
    }

    public function actionCreate()
    {
        $model = new Arbitr();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Arbitr::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
