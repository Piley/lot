<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = Yii::$app->params['meta']['title']['tariffs'];
$this->registerMetaTag(['name' => 'description', 'content' => Yii::$app->params['meta']['desc']['tariffs']]);

$this->params['breadcrumbs'][] = 'Тарифы';

?>
<div class="tariff-index">

<h1 class=""><?= Yii::$app->params['meta']['h1']['tariffs'] ?></h1>
<p class="lead">Получите доступ к полному функционалу нашего сервиса.</p>

<div class="row">
    <?php foreach ($tariffs as $model): ?>
    <div class="col-lg-3">
        <div class="panel panel-default">
            <div class="panel-body">
                <p class="tariff-block-time text-center">
                    <?= round($model->duration_time/2592000) ?>
                </p>
                <p class="tariff-block-month text-center">
                    <?= $model->description ?>
                </p>
                    <p class="tariff-block-price text-center">
                        <?= ceil($model->price) ?>
                        <span>руб.</span>
                    </p>
                    <hr>
                    <?php if (Yii::$app->user->isGuest): ?>
                        <a href="<?= Url::to(['/user/signup']); ?>" type="button" class="pay-button btn btn-block btn-primary">
                            <?php echo intval($model->price / round($model->duration_time/2592000)) ?>
                            руб. / месяц
                        </a>
                    <?php else: ?>
                        <button onclick="paymentForm(this)" data-id="<?= $model->id?>" type="button" class="pay-button btn btn-block btn-primary" data-toggle="modal" data-target="#choicePayType">
                            <?php echo intval($model->price / round($model->duration_time/2592000)) ?>
                            руб. / месяц
                        </button>
                    <?php endif ?>
            </div>
        </div>
    </div>
    <?php endforeach ?>
    </div>
        <br>    
        <br>
    <div class="row tariff-options">   
        <div class="col-lg-12"> 
            <p class="lead text-center">После подключения тарифа Вам будет доступен слудующий функционал</p>
                
            <div class="list-group">
              <div class="list-group-item">
              <img src="/img/tariffs/search.png" alt="">
                <h4 class="list-group-item-heading">Умный поиск</h4>
                <p class="list-group-item-text">Находить ликвидное имущество становится в разы проще!</p>
              </div>
              <div class="list-group-item">
                <img src="/img/tariffs/save.png" alt="">
                <h4 class="list-group-item-heading">Шаблоны поиска</h4>
                <p class="list-group-item-text">Вы сможете создать неограниченное количество шаблонов поиска. Сохраняйте выбранные фильтры и в следующий раз, вам не придется вводить их заново.</p>
              </div>
              <div class="list-group-item">
                <img src="/img/tariffs/glasses.png" alt="">
                <h4 class="list-group-item-heading">Мониторинг</h4>
                <p class="list-group-item-text">Следите за событиями по всем интересным лотам. Календарь упростит работы с планированием торгов.</p>
              </div>
              <div class="list-group-item">
                <img src="/img/tariffs/envelope.png" alt="">
                <h4 class="list-group-item-heading">Оповещения</h4>
                <p class="list-group-item-text">Включите функцию оповещения по email и система предупредит вас о приближении падения цен, начала торгов и прочих событий аукциона.</p>
              </div>
              <div class="list-group-item">
                <img src="/img/tariffs/books.png" alt="">
                <h4 class="list-group-item-heading">Реестры</h4>
                <p class="list-group-item-text">Полная информация о должниках, арбитражных управляющих и организаторах торгов. Просмотр последних торгов каждого контрагента.</p>
              </div>
            </div>

        </div>

        <div class="modal fade" id="choicePayType" tabindex="-1" role="dialog" aria-labelledby="choicePayType">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form id="payment-form" action="<?=Yii::$app->params['yandex']['payUrl']?>" method="post"> 
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Способ оплаты</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="quickpay-form" value="shop"> 
                        <input type="hidden" name="receiver" value="<?=Yii::$app->params['yandex']['receiver']?>"> 
                        <input id="payment-form-targets" type="hidden" name="targets" value="Тариф"> 
                        <input id="payment-form-sum" type="hidden" name="sum" value="" data-type="number"> 
                        <input id="payment-form-label" type="hidden" name="label" value=""> 

                        <p><label><input type="radio" checked name="paymentType" value="AC">Банковской картой</label></p>
                        <p><label><input type="radio" name="paymentType" value="PC">Яндекс.Деньгами</label></p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger btn-sm btn-block">Перейти к оплате</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    </div>


</div>
