<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\components\Alert;
use timurmelnikov\widgets\ShowLoading;
use app\assets\AppAsset;
raoul2000\bootswatch\BootswatchAsset::$theme = 'flatly';

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">

</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Html::img('@web/img/logo.png', ['alt'=>Yii::$app->name]),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-default navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Главная', 'url' => ['/site/index']],
        ['label' => 'Торги', 'url' => ['/trade/index']],
        [
            'label' => 'Инструменты', 
            'items' => [
                [
                    'label' => 'Менеджер торгов',
                    'url' => ['/manager/favorite'],
                ],
                [
                    'label' => 'Календарь событий',
                    'url' => ['/calendar/index'],
                ],
                [
                    'label' => 'Реестр должников',
                    'url' => ['/debtors/index'],
                ],
                [
                    'label' => 'Реестр организаторов торгов',
                    'url' => ['/organizers/index'],
                ],
                [
                    'label' => 'Реестр арбитражных управляющих',
                    'url' => ['/arbitr/index'],
                ],
            ],
        ],

        ['label' => 'Тарифы', 'url' => ['/tariffs/index']],
        ['label' => 'Блог', 'url' => ['/blog/index']],
    ];
    if (Yii::$app->user->can('admin_panel')) {
       $menuItems[] = [
            'label' => 'Админпанель', 
            'items' => [
                [
                    'label' => 'Пользователи',
                    'url' => ['/admin/user'],
                ],
                [
                    'label' => 'Платежи',
                    'url' => ['/admin/payments'],
                ],
            ],
        ];
    }
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Регистрация', 'url' => ['/user/signup']];
        $menuItems[] = ['label' => 'Вход', 'url' => ['/user/login']];
    } else {
        $avatar_label = Yii::$app->user->identity->avatar ? '@web/uploads/'.Yii::$app->user->identity->avatar : '@web/img/ava.svg';
        $menuItems[] = [
            'label' => Html::img($avatar_label, ['class' => 'img-rounded']),
            'items' => [
                [
                    'label' => 'Профиль',
                    'url' => ['/user'],
                ],
                '<li class="divider"></li>',
                [
                    'label' => 'Выход'.' (' . Yii::$app->user->identity->username . ')',
                    'url' => ['/user/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ],
            ],
        ];
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
        'encodeLabels' => false,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <div id="main-block">
            <?php echo ShowLoading::widget(['loadingType' => 1]); ?>
            <?= $content ?>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?=Yii::$app->name?> <?= date('Y') ?></p>
    </div>
</footer>

<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'mkry0gRr2p';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
