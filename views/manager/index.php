<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Tabs;
use yii\widgets\ListView;
use yii\widgets\Pjax;

$this->title = Yii::$app->params['meta']['title']['manager'];
$this->registerMetaTag(['name' => 'description', 'content' => Yii::$app->params['meta']['desc']['manager']]);

$this->params['breadcrumbs'][] = Yii::$app->params['meta']['h1']['manager'];

$checkController = function ($controller) {
   return $controller === Yii::$app->controller->action->id;
};

?>

<div class="lots-index">
    <?php Pjax::begin(['clientOptions' => ['method' => 'POST']]); ?>
    <h1><?= Html::encode(Yii::$app->params['meta']['h1']['manager']) ?></h1>
    <p class="lead">Списки отмеченных торгов.</p>
    <div class="row">
        <div class="col-lg-12">
                <? echo Tabs::widget([
                'items' => [
                    [
                        'label' => 'Избранные',
                        'content' => '',
                        'url' => Url::toRoute('manager/favorite'),
                        'active' => $checkController('favorite')
                    ],
                    [
                        'label' => 'Отслеживаемые',
                        'content' => '',
                        'url' => Url::toRoute('manager/monitor'),
                        'active' => $checkController('monitor')
                    ],
                    [
                        'label' => 'Мусор',
                        'content' => '',
                        'url' => Url::toRoute('manager/trash'),
                        'active' => $checkController('trash')
                    ],
                ],
            ]);
            ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <?php if (\app\models\User::isClient() ): ?>
                <div class="panel panel-default">
                    <div class="panel-body lots-list-panel">
                        
                    <?php echo ListView::widget([
                            'dataProvider' => $dataProvider,
                            'itemView' => $tab,
                            'summary'=>'', 
                            'sorter' => [],
                            'emptyText' => '<br>
                        <p class="lead text-center">Торги не найдены</p>',
                        ]);
                    ?>
                    
                    </div>
                </div>
            <?php else: ?>
                <?php echo $this->render('/site/noaccess'); ?>
            <?php endif ?>
            
        </div>
    </div>
    <?php Pjax::end(); ?>
</div>