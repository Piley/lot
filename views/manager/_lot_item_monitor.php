<?php
use yii\helpers\Url;
use yii\helpers\StringHelper;

?>
<div class="row lot-item">
    <div class="col-lg-5">
        <div class="lot-item-name">
            <a class="table-link" data-pjax="0" href="<?= Url::to(['trade/view', 'id' => $model->id]); ?>">
                Торги № <?= $model->id?> 
            </a>
        </div>
        <p>
            <span class="label label-primary">
            <?php if ($model->type_id == 1): ?>
                <i class="fa fa-arrow-down" style="color: orange;"></i> 
            <?php else: ?>
                <i class="fa fa-arrow-up" style="color: lime;"></i> 
            <?php endif ?>
                
                <?= number_format($model->now_price, 2, ',', ' ')?> руб.
            </span>
        </p>
        <p><?= StringHelper::truncate($model->description, 470, '...')?></p>
    </div>
    <div class="col-lg-3">
        <p><strong>Категория:</strong> <?= $model->category->name?></p>
        <p><strong>Регион:</strong> <?= $model->region->name?></p>
        <p><strong>Площадка:</strong> <?= $model->platform->name?></p>
    </div>
    <div class="col-lg-2">
        <p class="text-center">
            <?= Yii::$app->formatter->asDatetime($model->start_time, "php: d.m.y ") ?>
            -
            <?= Yii::$app->formatter->asDatetime($model->end_time, "php: d.m.y ") ?>
        </p>
        <p class="text-center pagado"><?= $model->status->name?></p>
    </div>
    <div class="col-lg-1"></div>
    <div class="col-lg-1">
        <div class="text-center bs-component">
            <button onclick="addMonitor(this)" data-id="<?= $model->id?>" type="button" class="add-monitor btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="" data-original-title="Следить">
                <i class="fa fa-search-minus"></i>
            </button>
        </div>
    </div>
</div>
