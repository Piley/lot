<?php

use app\models\User;
use yii\helpers\Html;
use yii\helpers\Url;

use yii\grid\GridView;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\helpers\StringHelper;

use app\components\CustomLinkPager;

$this->title = Yii::$app->params['meta']['title']['trade'];
$this->registerMetaTag(['name' => 'description', 'content' => Yii::$app->params['meta']['desc']['trade']]);

$this->params['breadcrumbs'][] =  Yii::$app->params['meta']['h1']['trade'];
?>
<div class="lots-index">
    <?php Pjax::begin(['clientOptions' => ['method' => 'POST']]); ?>    


    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-default">
              <div class="panel-body">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
              </div>

            </div>

        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <p class="total-count">Найдено по запросу: <strong><?=$total?></strong></p>
            <div class="panel panel-default">
                <div class="panel-body lots-list-panel">
                <?php if (count($lots) == 0): ?>
                    <br>
                    <p class="lead text-center">Торги не найдены</p>
                <?php endif ?>
                <?php foreach ($lots as $model): ?>
                    <div class="row lot-item">
                        <div class="col-lg-5">
                            <div class="lot-item-name">
                                <a data-pjax="0" class="table-link" href="<?= Url::to(['trade/view', 'id' => $model->id]); ?>">
                                    Торги № <?= $model->id?> 
                                </a>
                            </div>
                            <p>
                                <span class="label label-primary">
                                <?php if ($model->type_id == 1): ?>
                                    <i class="fa fa-arrow-down" style="color: orange;"></i> 
                                <?php else: ?>
                                    <i class="fa fa-arrow-up" style="color: lime;"></i> 
                                <?php endif ?>
                                    
                                    <?= number_format($model->now_price, 2, ',', ' ')?> руб.
                                </span>
                            </p>
                            <p><?= StringHelper::truncate($model->description, 470, '...')?></p>
                        </div>
                        <div class="col-lg-3">
                            <p><strong>Категория:</strong> <?= $model->category->name?></p>
                            <p><strong>Регион:</strong> <?= $model->region->name?></p>
                            <p>
                                <strong>Площадка:</strong> 
                                <?php if (User::isClient()): ?>
                                    <?= $model->platform->name?>
                                <?php else: ?>
                                    ▒▒▒▒▒▒▒▒
                                <?php endif ?>
                            </p>
                        </div>
                        <div class="col-lg-2">
                            <p class="text-center">
                                <?= Yii::$app->formatter->asDatetime($model->start_time, "php: d.m.y ") ?>
                                -
                                <?= Yii::$app->formatter->asDatetime($model->end_time, "php: d.m.y ") ?>
                            </p>
                            <p class="text-center pagado"><?= $model->status->name?></p>
                        </div>
                        <!-- <div class="col-lg-1"></div> -->
                        <div class="col-lg-2">
                        
                            <?php if (!Yii::$app->user->isGuest): ?>
                                <div class="text-center bs-component">
                                    <?php if ($model->lotFavorite): ?>
                                        <button onclick="addFavorite(this)" data-id="<?= $model->id?>" type="button" class="add-favorite btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="" data-original-title="Избранное">
                                            <i class="fa fa-minus"></i>
                                        </button>
                                    <?php else: ?>
                                        <button onclick="addFavorite(this)" data-id="<?= $model->id?>" type="button" class="add-favorite btn btn-xs btn-success" data-toggle="tooltip" data-placement="top" title="" data-original-title="Избранное">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    <?php endif ?>

                                    <?php if ($model->lotMonitor): ?>
                                        <button onclick="addMonitor(this)" data-id="<?= $model->id?>" type="button" class="add-monitor btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="" data-original-title="Следить">
                                            <i class="fa fa-search-minus"></i>
                                        </button>
                                    <?php else: ?>
                                        <button onclick="addMonitor(this)" data-id="<?= $model->id?>" type="button" class="add-monitor btn btn-xs btn-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Следить">
                                            <i class="fa fa-search-plus"></i>
                                        </button>
                                    <?php endif ?>

                                    <button onclick="addTrash(this)" data-id="<?= $model->id?>" type="button" class="add-trash btn btn-xs btn-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Мусор">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </div>
                            <?php endif ?>
                               

                        </div>
                    </div>


                    <?php endforeach ?>


                <?php
                    echo CustomLinkPager::widget([
                        'pagination' => $pages,
                    ]);
                ?>
                </div>

            </div>

        </div>
    </div>
    <?php Pjax::end(); ?>


</div>