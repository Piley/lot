<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use miloschuman\highcharts\Highcharts;
use yii\helpers\StringHelper;

$this->title = StringHelper::truncate($model->description, 50, '...').' - торги по банкротству | Madetrade.ru';
$this->registerMetaTag(['name' => 'description', 'content' => 'Торги по банкротству - '.StringHelper::truncate($model->description, 150, '...') ]);

$this->params['breadcrumbs'][] = ['label' => 'Торги', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Торги № '.$model->id;

?>

<div class="lot-index">

    <div class="row">
        <div class="col-lg-8">
            <div class="panel panel-default">
                <div class="panel-heading custom-panel-heading">
                    Торги № <?= $model->id ?>
                    <span class="general-price pull-right ">
                        <?php if ($model->type_id == 1): ?>
                            <i class="fa fa-arrow-down" style="color: red;"></i> 
                        <?php else: ?>
                            <i class="fa fa-arrow-up" style="color: green;"></i>
                        <?php endif ?>
                        <?= number_format($model->now_price, 2, ',', ' ')?> 
                        <i class="fa fa-rub"></i>
                    </span>
                </div>
                <div class="panel-body">
                    <p><?= $model->description ?></p>

                    <?php if (!Yii::$app->user->isGuest): ?>
                    <div class="bs-component pull-left">
                        <?php if ($model->lotFavorite): ?>
                            <button onclick="addFavorite(this)" data-id="<?= $model->id?>" type="button" class="add-favorite btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="" data-original-title="Избранное">
                                <i class="fa fa-minus"></i>
                            </button>
                        <?php else: ?>
                            <button onclick="addFavorite(this)" data-id="<?= $model->id?>" type="button" class="add-favorite btn btn-xs btn-success" data-toggle="tooltip" data-placement="top" title="" data-original-title="Избранное">
                                <i class="fa fa-plus"></i>
                            </button>
                        <?php endif ?>

                        <?php if ($model->lotMonitor): ?>
                            <button onclick="addMonitor(this)" data-id="<?= $model->id?>" type="button" class="add-monitor btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="" data-original-title="Следить">
                                <i class="fa fa-search-minus"></i>
                            </button>
                        <?php else: ?>
                            <button onclick="addMonitor(this)" data-id="<?= $model->id?>" type="button" class="add-monitor btn btn-xs btn-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Следить">
                                <i class="fa fa-search-plus"></i>
                            </button>
                        <?php endif ?>

                        <button onclick="addTrash(this)" data-id="<?= $model->id?>" type="button" class="add-trash btn btn-xs btn-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Мусор">
                            <i class="fa fa-trash"></i>
                        </button>
                    </div>
                    <?php endif ?>

                    <div class="pull-right">
                        <i class="fa fa-map-marker"></i>
                        <?=$model->region->name?>
                    </div>

                </div>
                <table class="table table-striped table-hover">
                    <colgroup> <col class="col-lg-3"> <col class="col-lg-9"> </colgroup>
                    <tbody>
                        <tr>
                            <th>Начальная цена</th>
                            <td><i class="fa fa-rub"></i> <?= number_format($model->start_price, 2, ',', ' ')?> </td>
                        </tr>
                        <tr>
                            <th>Текущая цена</th>
                            <td><i class="fa fa-rub"></i> <?= number_format($model->now_price, 2, ',', ' ')?> </td>
                        </tr>
<!--                         <tr>
                            <th>Рыночная цена</th>
                            <td><i class="fa fa-rub"></i> <?= number_format($model->market_price, 2, ',', ' ')?> </td>
                        </tr> -->
                        <tr>
                            <th>Задаток</th>
                            <td><i class="fa fa-rub"></i> <?= number_format($model->deposit_price, 2, ',', ' ')?> </td>
                        </tr>
                        <tr>
                            <th>Шаг аукциона</th>
                            <td><i class="fa fa-rub"></i><!-- <i class="fa fa-percent"></i> --> <?=$model->auct_step?></td>
                        </tr>
<!--                         <tr>
                            <th>Выгода</th>
                            <?php  
                                $profit = $model->market_price-$model->now_price;
                                if ($profit != 0) {
                                    $percent = $profit / ($model->now_price/100);
                                } else {
                                    $percent = 3434;
                                }
                            ?>
                            <td><i class="fa fa-arrow-up"></i> <?=number_format($profit, 2, ',', ' ')?> (<?=(int)$percent?>%)</td>
                        </tr> -->
                        <tr>
                            <th>Номер дела</th>
                            <td><i class="fa fa-book"></i> <?=$model->case_number?> </td>
                        </tr>
                        <tr>
                            <th>Ссылка на торги</th>
                            <td><i class="fa fa-external-link"></i> <a class="table-link" href="<?=$model->link?>"><?=$model->platform->name?></a></td>
                        </tr>
                    </tbody>
                </table>
            </div>


            <?php if ($model->organizer): ?>
            <!-- ОРГАНИЗАТОР ТОРГОВ -->
            <div class="panel panel-default">
                <div class="panel-heading custom-panel-heading">Организатор торгов</div>
                <table class="table table-striped table-hover">
                    <colgroup> <col class="col-lg-3"> <col class="col-lg-9"> </colgroup>
                    <tbody>
                        <tr>
                            <th>Наименование</th>
                            <td><a class="table-link" href="<?= Url::to(['organizers/view', 'id' => $model->organizer->id ]); ?>"><?= $model->organizer->short_name?> </a></td>
                        </tr>
                        <tr>
                            <th>ИНН</th>
                            <td><?= $model->organizer->inn?> </td>
                        </tr>
                        <tr>
                            <th>Адрес</th>
                            <td><i class="fa fa-map-marker"></i> <?= $model->organizer->address?> </td>
                        </tr>
                        <tr>
                            <th>Контактное лицо</th>
                            <td><i class="fa fa-user"></i> <?= $model->organizer->contact_person?></td>
                        </tr>
                        <tr>
                            <th>Телефон</th>
                            <td><i class="fa fa-phone"></i> <?= $model->organizer->phone?> </td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td><i class="fa fa-envelope"></i> <?= $model->organizer->email?> </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <?php endif ?>

            
            <?php if ($model->arbitr): ?>
            <!-- АРБИТРАЖНЫЙ УПРАВЛЯЮЩИЙ -->
            <div class="panel panel-default">
                <div class="panel-heading custom-panel-heading">Арбитражный управляющий</div>
                <table class="table table-striped table-hover">
                    <colgroup> <col class="col-lg-3"> <col class="col-lg-9"> </colgroup>
                    <tbody>
                        <tr>
                            <th>Наименование</th>
                            <td><a class="table-link" href="<?= Url::to(['arbitr/view', 'id' => $model->arbitr->id ]); ?>"><?= $model->arbitr->name?> </a></td>
                        </tr>
                        <tr>
                            <th>ИНН</th>
                            <td><?= $model->arbitr->inn?> </td>
                        </tr>
                        <tr>
                            <th>Адрес</th>
                            <td><i class="fa fa-map-marker"></i> <?= $model->arbitr->address?> </td>
                        </tr>
                        <tr>
                            <th>Контактное лицо</th>
                            <td><i class="fa fa-user"></i> <?= $model->arbitr->contact_person?></td>
                        </tr>
                        <tr>
                            <th>Телефон</th>
                            <td><i class="fa fa-phone"></i> <?= $model->arbitr->phone?> </td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td><i class="fa fa-envelope"></i> <?= $model->arbitr->email?> </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <?php endif ?>


            <?php if ($model->debtor): ?>
            <!-- ДОЛЖНИК -->
            <div class="panel panel-default">
                <div class="panel-heading custom-panel-heading">Банкрот</div>
                <table class="table table-striped table-hover">
                    <colgroup> <col class="col-lg-3"> <col class="col-lg-9"> </colgroup>
                    <tbody>
                        <tr>
                            <th>Наименование</th>
                            <td><a class="table-link" href="<?= Url::to(['debtors/view', 'id' => $model->debtor->id ]); ?>"><?= $model->debtor->short_name?> </a></td>
                        </tr>
                        <tr>
                            <th>ИНН</th>
                            <td><?= $model->debtor->inn?> </td>
                        </tr>
                        <tr>
                            <th>Адрес</th>
                            <td><i class="fa fa-map-marker"></i> <?= $model->debtor->address?> </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <?php endif ?>


        </div>
        <div class="col-lg-4">

            <!-- ДАТЫ -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>Информация</strong>
                </div>
                <?= DetailView::widget([
                    'model' => $model,
                    'options' => [
                        'class' => 'table table-striped table-hover',
                    ],
                    'attributes' => [
                        [
                            'attribute' => 'publish_time',
                            'value' => Yii::$app->formatter->asDatetime($model->publish_time, "php: d.m.Y  h:i"),
                        ], 
                        [
                            'attribute' => 'start_time',
                            'value' => Yii::$app->formatter->asDatetime($model->start_time, "php: d.m.Y  h:i"),
                        ], 
                        [
                            'attribute' => 'end_time',
                            'value' => Yii::$app->formatter->asDatetime($model->end_time, "php: d.m.Y  h:i"),
                        ], 
                        [
                            'attribute' => 'result_time',
                            'value' => Yii::$app->formatter->asDatetime($model->result_time, "php: d.m.Y  h:i"),
                        ], 
                    ],
                ]) ?>
            </div>


            <?php if ($intervals): ?>


            <div class="panel panel-default">
                <?php 
                    echo Highcharts::widget([
                       'options' => [
                            'legend' => ['enabled' => false],
                            'credits' => false,
                            'chart' => [
                                'type' => 'spline',
                            ],
                            'title' => ['text' => 'График понижения цены'],
                            'xAxis' => [
                                'categories' => array_column($intervals, 'start_time'),
                            ],
                            'yAxis' => [
                                'title' => ['text' => 'Цена на этапе']
                            ],
                            'series' => 
                            [
                                [
                                    'name' => 'Цена',
                                    'data' => array_column($intervals, 'price'),
                                ],
                                 // ['name' => 'Джон', 'data' => [5, 7, 3]]
                            ]
                       ]
                    ]);

                 ?>
            </div>

            <?php endif ?>


        </div>
    </div>

    <div class="row">
        <div class="col-lg-12"> 
            
        </div>
    </div>

</div>