<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

use vakorovin\datetimepicker\Datetimepicker;

use yii\helpers\ArrayHelper;
use app\models\Category;
use app\models\Region;
use app\models\Platform;
use app\models\Type;
use app\models\Status;
use app\models\LotTemplate;

?>

<div class="lot-search">

<?php $form = ActiveForm::begin([
    // 'action' => ['index'],
    'id' => 'lotsearch-form',
    'method' => 'post',
    'options' => ['data-pjax' => true ],
    'successCssClass' => '',
]); ?>

<div class="row">

    <!-- СТРОКА ПОИСКА -->
    <div class="col-lg-8">


    <?= $form->field($model, 'fullsearch')
        ->input('fullsearch', ['placeholder' => "название, город, банкрот, адрес, инн, номер дела..."]);
    ?>
    </div>
    

    <!-- ВЫБОР ЦЕН -->

    <div class="col-lg-2">
                
        <?= $form->field($model, 'from_price')
        ->input('from_price', ['placeholder' => "введите цену..."]);
        ?>
    </div>

    <div class="col-lg-2">
                
        <?= $form->field($model, 'to_price')
        ->input('to_price', ['placeholder' => "введите цену..."]);
        ?>
    </div>

    
</div> 


<div class="row">

    <!-- ТИП ТОРГОВ -->
    <div class="col-lg-2">
        <?= $form->field($model, 'types')
            ->checkboxList(
                 ArrayHelper::map(
                    Type::find()->asArray()->all(),
                    'id', 
                    'name'
                ),
                [
                'item'=>function ($index, $label, $name, $checked, $value){
                    $checked = $checked ? 'checked' : '';
                    return "<label><input type='checkbox' {$checked} name='{$name}' value='{$value}'> {$label}</label>";
                    }
                ]
                            
            );
        ?>
    </div>

    <!-- ВЫБОР СТАТУСОВ -->
    <div class="col-lg-4">
        <?= $form->field($model, 'statuses')
            ->checkboxList(
                 ArrayHelper::map(
                    Status::find()->asArray()->all(),
                    'id', 
                    'name'
                ),
                [
                'item'=>function ($index, $label, $name, $checked, $value){
                    $checked = $checked ? 'checked' : '';
                    return "<label><input type='checkbox' {$checked} name='{$name}' value='{$value}'> {$label}</label>";
                    }
                ]
                            
            );
        ?>
    </div>

    <!-- ВЫБОР ШАБЛОНА ПОИСКА -->
    <div class="col-lg-2">
        <?php if (!Yii::$app->user->isGuest): ?>
            <div id="select_lot_template">
            <?= $form->field($model, 'template')
                ->dropDownList(LotTemplate::getForList(Yii::$app->user->id));
             ?>
             </div>
        

        <!-- КНОПКИ УПРАВЛЕНИЯ ШАБЛОНАМИ -->
        <div class="text-center bs-component">
            <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#createTemplate">
            <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Создать новый шаблон">
                <i class="fa fa-file"></i>
            </span>
            </button>

            <button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#editTemplate">
            <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Редактировать шаблон">
                <i class="fa fa-edit"></i>
            </span>
            </button>
            
            <button onclick="deleteTemplate(this)" type="button" class="del-template btn btn-xs btn-danger">
            <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Удалить шаблон">
                <i class="fa fa-trash"></i>
            </span>
            </button>
        </div>

        <?php endif ?>

    </div>







    
    <!-- ВЫБОР ДАТ -->
    <div class="col-lg-2">
        <?= $form->field($model, 'from_date')->widget(Datetimepicker::className(),[
            'options' => [
                'lang'=>'ru', 
                'inline' => false,
                'format' =>'d.m.Y',
                'timepicker' => false,
            ]
        ]);
        ?>
    </div>
    <div class="col-lg-2">
                
        <?= $form->field($model, 'to_date')->widget(Datetimepicker::className(),[
            'options' => [
                'lang'=>'ru', 
                'inline' => false,
                'format' =>'d.m.Y',
                'timepicker' => false,
            ]
        ]);
        ?>
    </div>
    

</div>



<div class="row">
    
    <div class="col-lg-8 filter-choice">

        <!-- ВЫБОР КАТЕГОРИИ -->
        <button class="btn btn-xs btn-default" type="button" data-toggle="modal" data-target="#choiceCategory" aria-expanded="false" aria-controls="choiceCategory">
            <span class="glyphicon glyphicon-cog"></span>
            Категории
        </button>

        <div class="modal fade" id="choiceCategory" tabindex="-1" role="dialog" aria-labelledby="choiceCategory">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Выбор категорий</h4>
                    </div>
                    <div class="modal-body">
                        <div class="well-filter">
                        <div class="bs-component">
                            <button onclick="choiceAllCategories()" type="button" class="btn btn-primary btn-xs btn-choice-all">
                                Выбрать все
                            </button>

                            <button onclick="cancelAllCategories()" type="button" class="btn btn-default btn-xs btn-cancel-all">
                                Очистить
                            </button>
                            
                        </div>
                        <?php 
                            echo $form->field($model, 'categories')
                                ->checkboxList(
                                     ArrayHelper::map(
                                        Category::find()->asArray()->all(),
                                        'id', 
                                        'name'
                                    ),
                                    [
                                    'item'=>function ($index, $label, $name, $checked, $value){
                                        $checked = $checked ? 'checked' : '';
                                        return "<p><label><input type='checkbox' {$checked} name='{$name}' value='{$value}'> {$label}</label></p>";
                                        }
                                    ]
                                                
                                )->label(false);

                            ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary " data-dismiss="modal">Сохранить</button>
                    </div>
                </div>
            </div>
        </div>




        <!-- ВЫБОР РЕГИОНА -->
        <button class="btn btn-xs btn-default" type="button" data-toggle="modal" data-target="#choiceRegion" aria-expanded="false" aria-controls="choiceRegion">
            <span class="glyphicon glyphicon-cog"></span>
            Регионы
        </button>

        <div class="modal fade" id="choiceRegion" tabindex="-1" role="dialog" aria-labelledby="choiceRegion">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Выбор регионов</h4>
                    </div>
                    <div class="modal-body">
                        <div class="well-filter">
                        <div class="bs-component">
                            <button onclick="choiceAllRegions()" type="button" class="btn btn-primary btn-xs btn-choice-all">
                                Выбрать все
                            </button>

                            <button onclick="cancelAllRegions()" type="button" class="btn btn-default btn-xs btn-cancel-all">
                                Очистить
                            </button>
                            
                        </div>
                        <?php 
                            echo $form->field($model, 'regions')
                                ->checkboxList(
                                     ArrayHelper::map(
                                        Region::find()->asArray()->all(),
                                        'id', 
                                        'name'
                                    ),
                                    [
                                    'item'=>function ($index, $label, $name, $checked, $value){
                                        $checked = $checked ? 'checked' : '';
                                        return "<p><label><input type='checkbox' {$checked} name='{$name}' value='{$value}'> {$label}</label></p>";
                                        }
                                    ]
                                                
                                )->label(false);

                            ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary " data-dismiss="modal">Сохранить</button>
                    </div>
                </div>
            </div>
        </div>



        <!-- ВЫБОР ПЛОЩАДКИ -->
        <button class="btn btn-xs btn-default" type="button" data-toggle="modal" data-target="#choicePlatform" aria-expanded="false" aria-controls="choicePlatform">
            <span class="glyphicon glyphicon-cog"></span>
            Площадки
        </button>

        <div class="modal fade" id="choicePlatform" tabindex="-1" role="dialog" aria-labelledby="choicePlatform">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Выбор площадок</h4>
                    </div>
                    <div class="modal-body">
                        <div class="well-filter">
                        <div class="bs-component">
                            <button onclick="choiceAllPlatforms()" type="button" class="btn btn-primary btn-xs btn-choice-all">
                                Выбрать все
                            </button>

                            <button onclick="cancelAllPlatforms()" type="button" class="btn btn-default btn-xs btn-cancel-all">
                                Очистить
                            </button>
                            
                        </div>
                        <?php 
                            echo $form->field($model, 'platforms')
                                ->checkboxList(
                                    ArrayHelper::map(
                                        Platform::find()->asArray()->all(),
                                        'id', 
                                        'name'
                                    ),
                                    [
                                    'item'=>function ($index, $label, $name, $checked, $value){
                                        $checked = $checked ? 'checked' : '';
                                        return "<p><label><input type='checkbox' {$checked} name='{$name}' value='{$value}'> {$label}</label></p>";
                                        }
                                    ]
                                                
                                )->label(false);

                            ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary " data-dismiss="modal">Сохранить</button>
                    </div>
                </div>
            </div>
        </div>


    </div>
    


    <!-- СОРТИРОВКА -->
    <div class="col-lg-4">
    <?= $form->field($model, 'sort')
        ->dropDownList(
            [
                'load_time' => 'По дате добавления',
                'start_time' => 'По дате начала',
                'end_time' => 'По дате окончания',
                'start_price' => 'По начальной цене',
                'now_price' => 'По текущей цене',
            ]
        )->label(false);
    ?>
    </div>

    <input type="hidden" id="pagination-input" name="page" value="<?=Yii::$app->request->post('post')?>">

</div>




<hr>



    <div class="form-group">
        <?= Html::submitButton('<span class="glyphicon glyphicon-search"></span> Искать', ['class' => 'btn btn-warning btn-sm']) ?>
        <?= Html::button('<span class="glyphicon glyphicon-download-alt"></span> Экспорт в Excel', ['id' => 'lot-search-export','class' => 'btn btn-default btn-sm']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    
</div>





<!-- МОДАЛЬНЫЕ ОКНА УПРАВЛЕНИЯ ШАБЛОНАМИ -->

    <div class="modal fade" id="createTemplate" tabindex="-1" role="dialog" aria-labelledby="createTemplateLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="createTemplateLabel">Новый шаблон</h4>
                </div>
                <div class="modal-body">
                    <form>
                      <div class="form-group">
                        <label for="template-name" class="control-label">Название:</label>
                        <input type="text" class="form-control" id="template-create-name">
                      </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                    <button onclick="createTemplate(this)" type="button" class="btn btn-primary" data-dismiss="modal">Создать</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editTemplate" tabindex="-1" role="dialog" aria-labelledby="editTemplateLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="editTemplateLabel">Редактирование шаблона</h4>
                </div>
                <div class="modal-body">
                    <form>
                      <div class="form-group">
                        <label for="template-name" class="control-label">Название:</label>
                        <input type="text" class="form-control" id="template-edit-name">
                      </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                    <button onclick="editTemplate(this)" type="button" class="btn btn-primary" data-dismiss="modal">Сохранить</button>
                </div>
            </div>
        </div>
    </div>
