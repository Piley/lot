<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = Yii::$app->params['meta']['title']['calendar'];
$this->registerMetaTag(['name' => 'description', 'content' => Yii::$app->params['meta']['desc']['calendar']]);

$this->params['breadcrumbs'][] = Yii::$app->params['meta']['h1']['calendar'];
?>
<div class="task-index">

    <h1><?= Html::encode(Yii::$app->params['meta']['h1']['calendar']) ?></h1>
    <p class="lead">Здесь отображаются события по отслеживаемым Вами торгам.</p>
    
    <div class="row">
        <div class="col-lg-3">
            <p><span class="cal-notice-dot" style="background-color:#006600"></span>Начало подачи заявок</p>
        </div>
        <div class="col-lg-3">
            <p><span class="cal-notice-dot" style="background-color:#993333"></span>Окончание подачи заявок</p>
        </div>
        <div class="col-lg-3">
            <p><span class="cal-notice-dot" style="background-color:#426cc8"></span>Проведение торгов</p>
        </div>
        <div class="col-lg-3">
            <p><span class="cal-notice-dot" style="background-color:#9966cc"></span>Падение цены</p>
        </div>
    </div>
    

    <div class="row">
        <div class="col-lg-12">
            <?php if (\app\models\User::isClient() ): ?>
                
            <div class="panel panel-default">
                <div class="panel-body">
                <?= yii2fullcalendar\yii2fullcalendar::widget([
                        'defaultView'=>'listYear',
                        'header' => [
                            'center'=>'title',
                            'left'=>'prev,next today',
                            'right'=>'listYear,month,basicWeek,basicDay '
                        ],
                        'clientOptions' => [
                            'editable' => true,
                        ],
                        'options' => [
                            'lang' => 'ru',
                        ],
                        'ajaxEvents' => Url::to(['calendar/jsoncalendar'])
                    ]);
                ?>

                </div>
                <div class="panel-footer">
                    <?php if (Yii::$app->user->identity->notice): ?>
                        <button onclick="activateNotice(this)" type="button" class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="" data-original-title="Включить ежедневную рассылку о ближайших событиях по торгам">
                            <i class="fa fa-check"></i> Отключить рассылку
                        </button>
                    <?php else: ?>
                        <button onclick="activateNotice(this)" type="button" class="btn btn-xs btn-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Включить ежедневную рассылку о ближайших событиях по торгам">
                            <i class="fa fa-envelope-o"></i> Ежедневная рассылка
                        </button>
                    <?php endif ?>
                </div>
                <?php else: ?>
                    <?php echo $this->render('/site/noaccess'); ?>
                <?php endif ?>
            </div>
        </div>
    </div>

</div>