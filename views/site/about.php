<?php

use yii\helpers\Html;

$this->title = Yii::$app->params['meta']['title']['about'];
$this->registerMetaTag(['name' => 'description', 'content' => Yii::$app->params['meta']['desc']['about']]);

$this->params['breadcrumbs'][] = Yii::$app->params['meta']['h1']['about'];
?>
<div class="site-about">
    <h1><?= Html::encode(Yii::$app->params['meta']['h1']['about']) ?></h1>

    <p>
        This is the About page. You may modify the following file to customize its content:
    </p>

    <code><?= __FILE__ ?></code>
</div>
