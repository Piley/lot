<?php 

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Профиль';

$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row">
    <div class="col-lg-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                Профиль
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-3">
                        <p>
                            <?php $avatar_label = Yii::$app->user->identity->avatar ? '@web/uploads/'.$user->avatar : '@web/img/ava-profile.svg'; ?>
                            <?=Html::img( $avatar_label, ['class' => 'center-block img-responsive img-thumbnail'])?>
                        </p>
                        <div class="list-group">
                          <a href="<?= Url::to(['/user/']); ?>" class="list-group-item">Профиль</a>
                          <a href="<?= Url::to(['/user/avatar']); ?>" class="list-group-item">Изменить аватар</a>
                          <a href="<?= Url::to(['/user/password']); ?>" class="list-group-item">Изменить пароль</a>
                          <a href="<?= Url::to(['/user/email']); ?>" class="list-group-item">Изменить email</a>
                          <a href="<?= Url::to(['/user/promo']); ?>" class="list-group-item">Ввести промо-код</a>
                        </div>
                        
                    </div>

                    
                    <div class="col-lg-9">
                        <h2><?=$user->username?></h2>
                        <hr>
                        <?php $form = ActiveForm::begin(); ?>
     
                        <?= $form->field($model, 'currentPassword')->passwordInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'newPassword')->passwordInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'newPasswordRepeat')->passwordInput(['maxlength' => true]) ?>
                 
                        <div class="form-group">
                            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>


                    </div>
                </div>
            </div>

        </div>

    </div>

</div>



