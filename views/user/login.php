<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::$app->params['meta']['title']['login'];
$this->registerMetaTag(['name' => 'description', 'content' => Yii::$app->params['meta']['desc']['login']]);

$this->params['breadcrumbs'][] = Yii::$app->params['meta']['h1']['login'];
?>


<div class="row row-centered">
    <div class="col-lg-5 col-centered">
        <h1><?= Html::encode(Yii::$app->params['meta']['h1']['login']) ?></h1>

        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?= $form->field($model, 'rememberMe')->checkbox() ?>

            <div style="color:#999;margin:1em 0">
                Если вы забыли свой пароль вы можете <?= Html::a('восстановить его', ['user/request-password-reset']) ?>.
            </div>

            <div class="text-center form-group">
                <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

