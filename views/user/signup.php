<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = Yii::$app->params['meta']['title']['signup'];
$this->registerMetaTag(['name' => 'description', 'content' => Yii::$app->params['meta']['desc']['signup']]);

$this->params['breadcrumbs'][] = Yii::$app->params['meta']['h1']['signup'];
?>

<div class="row row-centered">
    <div class="col-lg-5 col-centered">
        <h1><?= Html::encode(Yii::$app->params['meta']['h1']['signup']) ?></h1>
        <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'email') ?>

            <?= $form->field($model, 'password')->passwordInput() ?>
            
            <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-lg-4">{image}</div><div class="col-lg-8">{input}</div></div>',
                ]) ?>
            <hr>
            <div class="text-center form-group">
                <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
