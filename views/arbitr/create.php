<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Arbitr */

$this->title = 'Create Arbitr';
$this->params['breadcrumbs'][] = ['label' => 'Arbitrs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="arbitr-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
