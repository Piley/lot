<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = Yii::$app->params['meta']['title']['arbitrs'];
$this->registerMetaTag(['name' => 'description', 'content' => Yii::$app->params['meta']['desc']['arbitrs']]);

$this->params['breadcrumbs'][] = Yii::$app->params['meta']['h1']['arbitrs'];
?>
<div class="arbitr-index">
<h1><?= Html::encode(Yii::$app->params['meta']['h1']['arbitrs']) ?></h1>
<div class="row">
    <div class="col-lg-12">
        <?php if (\app\models\User::isClient() ): ?>
            <div class="panel panel-default">
                <div class="panel-body">
                <?php Pjax::begin(); ?>    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'name',
                            'inn',
                            // 'email:email',
                            // 'phone',
                            // 'fedresurs_link',
                            // 'sro_link',
                            // 'reg_number',
                            // 'reg_time:datetime',
                            // 'begin_time:datetime',

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{view}',
                            ],
                        ],
                    ]); ?>
                <?php Pjax::end(); ?>
                </div>
            </div>
        
        <?php else: ?>
            <?php echo $this->render('/site/noaccess'); ?>
        <?php endif ?>



    </div>
</div>


</div>

