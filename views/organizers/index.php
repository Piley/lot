<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = Yii::$app->params['meta']['title']['organizers'];
$this->registerMetaTag(['name' => 'description', 'content' => Yii::$app->params['meta']['desc']['organizers']]);

$this->params['breadcrumbs'][] = Yii::$app->params['meta']['h1']['organizers'];
?>
<div class="organizer-index">

<h1><?= Html::encode(Yii::$app->params['meta']['h1']['organizers']) ?></h1>
<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<div class="row">
    <div class="col-lg-12">
        <?php if (\app\models\User::isClient() ): ?>
                
        <div class="panel panel-default">
            <div class="panel-body">
            <?php Pjax::begin(); ?>    <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'short_name',
                        'inn',
                        'address',
                        'phone',
                        // 'email:email',
                        // 'fedresurs_link',

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view}',
                        ],
                    ],
                ]); ?>
            <?php Pjax::end(); ?>
            </div>
        </div>
        
        <?php else: ?>
            <?php echo $this->render('/site/noaccess'); ?>
        <?php endif ?>
        
    </div>
</div>



</div>