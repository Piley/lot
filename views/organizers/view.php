<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\helpers\StringHelper;

$this->title = $model->short_name;
$this->params['breadcrumbs'][] = ['label' => 'Организаторы торгов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="organizer-view">

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <h1 class="custom-header"><?= Html::encode($this->title) ?></h1>
                
                <table class="table table-striped table-bordered">
                    <colgroup> <col class="col-lg-3"> <col class="col-lg-9"> </colgroup>
                    <tbody>
                        <tr>
                            <th>Полное наименование</th>
                            <td><?=$model->full_name?> </td>
                        </tr>
                        <tr>
                            <th>Регион</th>
                            <td><?=$model->region->name?> </td>
                        </tr>
                        <tr>
                            <th>Адрес</th>
                            <td><?=$model->address?> </td>
                        </tr>
                        <tr>
                            <th>Телефон</th>
                            <td><?=$model->phone ? $model->phone : 'Неизвестно'?> </td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td><?=$model->email ? $model->email : 'Неизвестно'?> </td>
                        </tr>
                        <tr>
                            <th>ИНН</th>
                            <td><?=$model->inn?> </td>
                        </tr>
                        <tr>
                            <th>ОГРН</th>
                            <td><?=$model->ogrn ? $model->ogrn : 'Неизвестно'?> </td>
                        </tr>
                        <tr>
                            <th>Ссылка на федресурс</th>
                            <td><i class="fa fa-external-link"></i> <a class="table-link" href="<?=$model->fedresurs_link?>"><?=$model->short_name?></a></td>
                        </tr>
                    </tbody>
                </table>


            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
         <?php if ($lots): ?>
        <div class="panel panel-default">
            <div class="panel-heading custom-panel-heading">Последние торги</div>
            <div class="panel-body lots-list-panel">
           
            <?php foreach ($lots as $model): ?>

                <div class="row lot-item">
                    <div class="col-lg-5">
                        <div class="lot-item-name">
                            <a data-pjax="0" class="table-link" href="<?= Url::to(['trade/view', 'id' => $model->id]); ?>">
                                Торги № <?= $model->id?> 
                            </a>
                        </div>
                        <p>
                            <span class="label label-primary">
                            <?php if ($model->type_id == 1): ?>
                                <i class="fa fa-arrow-down" style="color: orange;"></i> 
                            <?php else: ?>
                                <i class="fa fa-arrow-up" style="color: lime;"></i> 
                            <?php endif ?>
                                
                                <?= number_format($model->now_price, 2, ',', ' ')?> руб.
                            </span>
                        </p>
                        <p><?= StringHelper::truncate($model->description, 470, '...')?></p>
                    </div>
                    <div class="col-lg-3">
                        <p class="text-center pagado">Категория: <?= $model->category->name?></p>
                        <p class="text-center pagado">Регион: <?= $model->region->name?></p>
                        <p class="text-center pagado">Площадка: <?= $model->platform->name?></p>
                    </div>
                    <div class="col-lg-2">
                        <p class="text-center">
                            <?= Yii::$app->formatter->asDatetime($model->start_time, "php: d.m.y ") ?>
                            -
                            <?= Yii::$app->formatter->asDatetime($model->end_time, "php: d.m.y ") ?>
                        </p>
                        <p class="text-center pagado"><?= $model->status->name?></p>
                    </div>
                    <!-- <div class="col-lg-1"></div> -->
                    <div class="col-lg-2">
                    
                        <?php if (!Yii::$app->user->isGuest): ?>
                            <div class="text-center bs-component">
                                <?php if ($model->lotFavorite): ?>
                                    <button onclick="addFavorite(this)" data-id="<?= $model->id?>" type="button" class="add-favorite btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="" data-original-title="Избранное">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                <?php else: ?>
                                    <button onclick="addFavorite(this)" data-id="<?= $model->id?>" type="button" class="add-favorite btn btn-xs btn-success" data-toggle="tooltip" data-placement="top" title="" data-original-title="Избранное">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                <?php endif ?>

                                <?php if ($model->lotMonitor): ?>
                                    <button onclick="addMonitor(this)" data-id="<?= $model->id?>" type="button" class="add-monitor btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="" data-original-title="Следить">
                                        <i class="fa fa-search-minus"></i>
                                    </button>
                                <?php else: ?>
                                    <button onclick="addMonitor(this)" data-id="<?= $model->id?>" type="button" class="add-monitor btn btn-xs btn-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Следить">
                                        <i class="fa fa-search-plus"></i>
                                    </button>
                                <?php endif ?>

                                <button onclick="addTrash(this)" data-id="<?= $model->id?>" type="button" class="add-trash btn btn-xs btn-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Мусор">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                        <?php endif ?>
                           

                    </div>
                </div>


                <?php endforeach ?>

            </div>
            <?php endif ?>


        </div>

    </div>
</div>








</div>
