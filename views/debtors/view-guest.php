<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\helpers\StringHelper;

$this->title = $model->short_name;
$this->params['breadcrumbs'][] = ['label' => 'Должники', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="debtor-view">

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
            <h1 class="custom-header"><?= Html::encode($this->title) ?></h1>


                <table class="table table-striped table-bordered">
                    <colgroup> <col class="col-lg-3"> <col class="col-lg-9"> </colgroup>
                    <tbody>
                        <tr>
                            <th>Полное наименование</th>
                            <td><?=$model->full_name?> </td>
                        </tr>
                        <tr>
                            <th>Регион</th>
                            <td>▒▒▒▒▒▒▒▒ </td>
                        </tr>
                        <tr>
                            <th>Адрес</th>
                            <td>▒▒▒▒▒▒▒▒ </td>
                        </tr>
                        <tr>
                            <th>Телефон</th>
                            <td>▒▒▒▒▒▒▒▒ </td>
                        </tr>
                        <tr>
                            <th>ИНН</th>
                            <td>▒▒▒▒▒▒▒▒ </td>
                        </tr>
                        <tr>
                            <th>ОГРН</th>
                            <td>▒▒▒▒▒▒▒▒ </td>
                        </tr>
                        <tr>
                            <th>Ссылка на федресурс</th>
                            <td><i class="fa fa-external-link"></i> ▒▒▒▒▒▒▒▒</td>
                        </tr>
                    </tbody>
                </table>

                
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">

        <div class="panel panel-default">
            <div class="panel-heading custom-panel-heading">Последние торги</div>
            <div class="panel-body lots-list-panel">
                <p><?php echo $this->render('/site/noaccess'); ?></p>
            </div>
        </div>

    </div>
</div>



</div>

