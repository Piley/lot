<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;

$this->title = Yii::$app->params['meta']['title']['blog'];
$this->registerMetaTag(['name' => 'description', 'content' => Yii::$app->params['meta']['desc']['blog']]);

$this->params['breadcrumbs'][] = Yii::$app->params['meta']['h1']['blog'];
?>
<div class="blog-index">
    <h1><?= Html::encode(Yii::$app->params['meta']['h1']['blog']) ?></h1>
    <p class="lead">Полезные статьи о банкротстве и не только.</p>

    <div class="row">
        <div class="col-lg-9">
            <?php if (count($models) == 0): ?>
                <div class="panel panel-default">
                    <br>
                    <p class="lead text-center">Записи не найдены.</p>
                </div>
            <?php endif ?>

            <?php foreach ($models as $model): ?>
                
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4><?= Html::a($model->name, ['view', 'id' => $model->id]) ?></h4>
                        <p>
                            <span class="label label-default"><?= Yii::$app->formatter->asDate($model->create_time, 'php:d.m.Y'); ?></span>
                        </p>
                        <p><?=$model->description?></p>
                        <?= Html::a('Читать полностью', ['view', 'id' => $model->id ], ['class' => 'btn btn-primary btn-sm']) ?>
                    </div>
                </div>
            <?php endforeach ?>
            <?php 
                echo LinkPager::widget([
                    'pagination' => $pages,
                ]);
             ?>
        </div>
        
        <div class="col-lg-3">
            <div class="list-group">
                <a href="<?= Url::to(['blog/index']); ?>" class="list-group-item <?php if (!Yii::$app->request->get('category')):?>active<?php endif ?>">
                    <span class="badge"><?=$total?></span>
                    Все записи
                </a>
                
                <?php foreach ($cats as $cat): ?>
                    <a href="<?= Url::to(['blog/index', 'category' => $cat['id']]); ?>" class="list-group-item <?php if (Yii::$app->request->get('category') == $cat['id']):?>active<?php endif ?>">
                        <span class="badge"><?=$cat['cnt']?></span>
                        <?=$cat['name']?>
                    </a>
                <?php endforeach ?>

            </div>
        </div>
    </div>


</div>
