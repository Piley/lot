<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

$this->title = $model->name;
$this->registerMetaTag(['name' => 'description', 'content' => $model->description]);

$this->params['breadcrumbs'][] = ['label' => 'Блог', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-view">

   
    
    <div class="row">
        <div class="col-lg-9">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h1 class="custom-header"><?= Html::encode($this->title) ?></h1>
                    <p class="article-body"><?=$model->text?></p>
                    <p>
                        <span class="label label-default"><?= Yii::$app->formatter->asDate($model->create_time, 'php:d.m.Y'); ?></span>
                        <span class="label label-primary"><?=$model->category->name?></span>
                        <span class="label label-info"><?=$model->user->username?></span>
                    </p>
                </div>

                <!-- КОММЕНТАРИИ -->
                <div class="panel-body">
                
                    <?php if (!Yii::$app->user->isGuest): ?>
                        <?php $form = ActiveForm::begin(['method' => 'post',]); ?>
                        <div class="form-group">
                            <p class="custom-sub-header">Оставить комментарий:</p>
                            <textarea id="blogcomment-text" class="form-control" name="text" rows="3" aria-required="true" aria-invalid="true"></textarea>
                        </div>
                        <div class="form-group">
                            <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary btn-sm']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    <?php endif ?>

                    <?php foreach ($comments as $comment): ?>
                        <div class="media">
                          <div class="media-left">
                            <?=Html::img( $comment->user->avatar ? '@web/uploads/'.$comment->user->avatar : '@web/img/ava.jpg', ['class' => 'img-rounded media-object'])?>
                          </div>
                          <div class="media-body">
                            <p class="media-heading"><?=$comment->user->username?> <small class="text-muted"> <?= \Yii::$app->formatter->asDate($comment->create_time, 'php:d.m.Y'); ?></small></p>
                            <p><?=Html::encode($comment->text)?></p>
                        
                          </div>
                        </div>
                    <?php endforeach ?>

                </div>
            </div>


        </div>
        <div class="col-lg-3">
            <div class="list-group">
                <a href="<?= Url::to(['blog/index']); ?>" class="list-group-item">
                    <span class="badge"><?=$total?></span>
                    Все записи
                </a>
                
                <?php foreach ($cats as $cat): ?>
                    <a href="<?= Url::to(['blog/index', 'category' => $cat['id']]); ?>" class="list-group-item">
                        <span class="badge"><?=$cat['cnt']?></span>
                        <?=$cat['name']?>
                    </a>
                <?php endforeach ?>

            </div>
        </div>
    </div>



</div>
