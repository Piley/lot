<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Payment;
use yii\web\Controller;

class YandexController extends Controller
{
    public $layout=false;

    public function behaviors()
    {
        return [
            // 'verbs' => [
            //     'class' => VerbFilter::className(),
            //     'actions' => [
            //         'delete' => ['POST'],
            //     ],
            // ],
        ];
    }

    // отмена csrf валидации
    public function beforeAction($action)
    {
       $this->enableCsrfValidation = false;
       return parent::beforeAction($action);
    }

    // url для яндекс формы
    public function actionWallet()
    {
        $secret = Yii::$app->params['yandex']['shopKey']; // секрет, который мы получили в первом шаге от яндекс.
        // получение данных.
        $r = [
            'notification_type' => $_POST['notification_type'], // p2p-incoming / card-incoming - с кошелька / с карты
            'operation_id'      => $_POST['operation_id'],      // Идентификатор операции в истории счета получателя.
            'amount'            => $_POST['amount'],            // Сумма, которая зачислена на счет получателя.
            'withdraw_amount'   => $_POST['withdraw_amount'],   // Сумма, которая списана со счета отправителя.
            'currency'          => $_POST['intval'],            // Код валюты — всегда 643 (рубль РФ согласно ISO 4217).
            'datetime'          => $_POST['datetime'],          // Дата и время совершения перевода.
            'sender'            => $_POST['sender'],            // Для переводов из кошелька — номер счета отправителя. Для переводов с произвольной карты — параметр содержит пустую строку.
            'codepro'           => $_POST['codepro'],           // Для переводов из кошелька — перевод защищен кодом протекции. Для переводов с произвольной карты — всегда false.
            'label'             => $_POST['label'],             // Метка платежа. Если ее нет, параметр содержит пустую строку.
            'sha1_hash'         => $_POST['sha1_hash']          // SHA-1 hash параметров уведомления.
        ];

        // проверка хеш
        if (sha1($r['notification_type'].'&'.
                $r['operation_id'].'&'.
                $r['amount'].'&'.
                $r['currency'].'&'.
                $r['datetime'].'&'.
                $r['sender'].'&'.
                $r['codepro'].'&'.
                $secret.'&'.
                $r['label']) != $r['sha1_hash']) 
        {
            // отмена заказа
            $order = $this->findModel($r['label']);
            if ($order) {
                $order->status_id = Yii::$app->params['yandex']['status']['abort'];
                $order->save(false);
            }
            exit('Верификация не пройдена. SHA1_HASH не совпадает.'); // останавливаем скрипт. у вас тут может быть свой код.
        }

        // обработаем данные. нас интересует основной параметр label и withdraw_amount для получения денег без комиссии для пользователя.
        // либо если вы хотите обременить пользователя комиссией - amount, но при этом надо учесть, что яндекс на странице платежа будет писать "без комиссии".
        $r['amount']          = floatval($r['amount']);
        $r['withdraw_amount'] = floatval($r['withdraw_amount']);
        $r['label']           = intval($r['label']); // здесь я у себя передаю id юзера, который пополняет счет на моем сайте. поэтому обрабатываю его intval

        // подтверждение заказа
        $order = $this->findModel($r['label']);
        if ($order) {
            $order->status_id = Yii::$app->params['yandex']['status']['success'];
            $order->save(false);
        }

    }

    // основной url (checkOrder, paymentAviso)
    public function actionMainUrl()
    {
        $request = Yii::$app->request->post();
        $params = null;

        // если запрос не прошел проверку хеша
        if (!$this->checkMD5($request)) {
            $params = $this->buildResponseParams($request['action'], $request['invoiceId'], 1);
            
            $order = $this->findModel($request['orderNumber']);
            // проверка, есть ли заказ с указанным ID
            if ($order) {
                $order->status_id = Yii::$app->params['yandex']['status']['abort'];
                $order->save(false);
            }
            $params = $this->buildResponseParams($request['action'], $request['invoiceId'], 1);

            return $this->render('response_xml', $params);
        }

        if ($request['action'] == 'checkOrder') {
            $params = $this->checkOrder($request);
        } elseif ($request['action'] == 'paymentAviso') {
            $params = $this->paymentAviso($request);
        } elseif ($request['action'] == 'cancelOrder') {
            $params = $this->paymentAviso($request);
        } else {
            $params = $this->buildResponseParams($request['action'], $request['invoiceId'], 100, "Неверный метод.");
        }

        return $this->render('response_xml', $params);
    }

    // подтверждение заказа
    protected function checkOrder($request)
    {
        $params = null;

        $order = $this->findModel($request['orderNumber']);
        // проверка, есть ли заказ с указанным ID
        if ($order) {
            $order->status_id = Yii::$app->params['yandex']['status']['checked'];
            $order->save(false);
            $params = $this->buildResponseParams($request['action'], $request['invoiceId'], 0);
        } else {
            $params = $this->buildResponseParams($request['action'], $request['invoiceId'], 100, "Неверный номер заказа.");
        }

        return $params;
    }

    // успешный заказ
    protected function paymentAviso($request)
    {
        $params = null;

        $order = $this->findModel($request['orderNumber']);
        // проверка, есть ли заказ с указанным ID
        if ($order) {
            $order->status_id = Yii::$app->params['yandex']['status']['success'];
            $order->giveAccess($order);
            $order->success_time = time();
            $order->save(false);

            $params = $this->buildResponseParams($request['action'], $request['invoiceId'], 0);
        } else {
            $params = $this->buildResponseParams($request['action'], $request['invoiceId'], 100, "Неверный номер заказа.");
        }

        return $params;
    }

    // отмена заказа (*если на стороне яндекса клиент захотел кредит, но ему не дали)
    protected function cancelOrder($request)
    {
        $params = null;

        $order = $this->findModel($request['orderNumber']);
        // проверка, есть ли заказ с указанным ID
        if ($order) {
            $order->status_id = Yii::$app->params['yandex']['status']['abort_ya'];
            $order->save(false);
            $params = $this->buildResponseParams($request['action'], $request['invoiceId'], 0);
        } else {
            $params = $this->buildResponseParams($request['action'], $request['invoiceId'], 100, "Неверный номер заказа.");
        }

        return $params;
    }

    // формирование параметров для xml ответа
    protected function buildResponseParams($action = 'checkOrder', $invoiceId, $result_code, $message = null) {
        $datetime = new \DateTime();
        $performedDatetime = $datetime->format('c');

        $response_params = [
            'action' => $action ? $action : 'checkOrder',
            'performedDatetime' => $performedDatetime,
            'result_code' => $result_code,
            'message' => $message,
            'invoiceId' => $invoiceId,
        ];

        return $response_params;
    }

    // проверка хеша MD5
    protected function checkMD5($request) 
    {
        $str = $request['action'] . ";" .
            $request['orderSumAmount'] . ";" . $request['orderSumCurrencyPaycash'] . ";" .
            $request['orderSumBankPaycash'] . ";" . $request['shopId'] . ";" .
            $request['invoiceId'] . ";" . $request['customerNumber'] . ";" 
            . Yii::$app->params['yandex']['shopPassword'];
        
        $md5 = strtoupper(md5($str));
        if ($md5 != strtoupper($request['md5'])) {
            return false;
        }
        return true;
    }

    protected function findModel($id)
    {
        return Payment::findOne($id);
    }
}
