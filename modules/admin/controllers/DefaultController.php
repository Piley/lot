<?php

namespace app\modules\admin\controllers;

use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    // только для ролей с доступом к админке
                    [
                        'allow' => true,
                        'roles' => ['admin_panel'],
                    ],
                ],
            ],        
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
}
