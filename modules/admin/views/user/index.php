<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        'username',
        'email:email',
        'created_at:date',
        'updated_at:date',
        [
            'attribute'=>'role',
            'filter'=> [
                '10'=>'Пользователь',
                '20'=>'Клиент',
                '50'=>'Модератор',
                '100'=>'Администратор',
            ],
            'value' => function ($model){
                switch ($model->role) {
                    case 10:
                       return 'Пользователь';
                    case 20:
                       return 'Клиент';
                    case 50:
                        return 'Модератор';
                    case 100:
                        return 'Администратор';
                    default:
                        return 'Неизвестно';
                }
            },
        ],
        'tariff_time:date',
        ['class' => 'yii\grid\ActionColumn'],
    ],
]); ?>
<?php Pjax::end(); ?>

</div>
