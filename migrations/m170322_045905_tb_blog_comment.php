<?php

use yii\db\Migration;

class m170322_045905_tb_blog_comment extends Migration
{
    public function up()
    {
        $sql = "CREATE TABLE IF NOT EXISTS tb_blog_comment ( 
            id int(10) NOT NULL AUTO_INCREMENT, 
            user_id int(11) NOT NULL DEFAULT '0', 
            blog_id int(10) NOT NULL DEFAULT '0',
            text text NOT NULL DEFAULT '',
            create_time int(10) NOT NULL DEFAULT '0',

            PRIMARY KEY (id)
        );";

        $this->execute($sql);
        echo "m170322_045905_tb_blog_comment successfully applied.\n";
    }

    public function down()
    {
        echo "m170322_045905_tb_blog_comment cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
