<?php

use yii\db\Migration;

class m170302_131602_tb_lot_trash extends Migration
{
    public function up()
    {
        $sql = "CREATE TABLE IF NOT EXISTS tb_lot_trash ( 
            id int(10) NOT NULL AUTO_INCREMENT, 
            lot_id int(10) NOT NULL DEFAULT '0', 
            user_id int(11) NOT NULL DEFAULT '0', 
            add_time int(10) NOT NULL DEFAULT '0', 

            PRIMARY KEY (id)
        );"; 

        $this->execute($sql);
        echo "m170302_131602_tb_lot_trash successfully applied.\n";
    }

    public function down()
    {
        echo "m170302_131602_tb_lot_trash cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
