<?php

use yii\db\Migration;

class m170503_211450_fed_trade extends Migration
{
    public function up()
    {
        $sql = "CREATE TABLE IF NOT EXISTS fed_trade ( 
            id int(10) NOT NULL AUTO_INCREMENT, 
            trade_id int(10) NOT NULL DEFAULT '0' COMMENT 'Номер в ссылке на федресурсе',
            trade_number varchar(128) NOT NULL DEFAULT '' COMMENT 'Номер торгов на площадке',
            platform_link varchar(128) NOT NULL DEFAULT '' COMMENT 'Страница площадки на федресурсе', 
            debtor_link varchar(128) NOT NULL DEFAULT '' COMMENT 'Страница должника на федресурсе', 
            debtor_id int(10) NOT NULL DEFAULT '0' COMMENT 'Должник', 
            arbitr_link varchar(128) NOT NULL DEFAULT '' COMMENT 'Страница арбитражника на федресурсе', 
            arbitr_id int(10) NOT NULL DEFAULT '0' COMMENT 'Арбитражник', 
            organizer_link varchar(128) NOT NULL DEFAULT '' COMMENT 'Страница организатора на федресурсе', 
            organizer_id int(10) NOT NULL DEFAULT '0' COMMENT 'Организатор', 
            
            message_link varchar(128) NOT NULL DEFAULT '' COMMENT 'Ссылка на сообщение', 
            message_number int(10) NOT NULL DEFAULT '0' COMMENT 'Номер сообщения', 
            message_time int(10) NOT NULL DEFAULT '0' COMMENT 'Время публикации', 

            is_parsed tinyint(1) NOT NULL DEFAULT '0' COMMENT 'парсился = 1 не парсился = 0',
            PRIMARY KEY (id)
        );";
        
        $this->execute($sql);
    }

    public function down()
    {
        echo "m170503_211450_fed_trade cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
