<?php

use yii\db\Migration;

class m170228_030941_alt_user_add_avatar extends Migration
{
    public function up()
    {
        $sql = "ALTER TABLE tb_user ADD avatar varchar(256) NOT NULL DEFAULT '';";
        $this->execute($sql);
        echo "m170228_030941_alt_user_add_avatar successfully applied.\n";
    }

    public function down()
    {
        echo "m170228_030941_alt_user_add_avatar cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
