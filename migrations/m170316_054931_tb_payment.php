<?php

use yii\db\Migration;

class m170316_054931_tb_payment extends Migration
{
    public function up()
    {
        $sql = "CREATE TABLE IF NOT EXISTS tb_payment ( 
            id int(10) NOT NULL AUTO_INCREMENT, 
            user_id int(11) NOT NULL DEFAULT '0' COMMENT 'Пользователь (tb_user)', 
            tariff_id int(3) NOT NULL DEFAULT '0' COMMENT 'Тариф (tb_tariff)', 
            invoice_id bigint(20) NOT NULL DEFAULT '0' COMMENT 'Номер транзакции (Яндекс)', 
            status_id int(3) NOT NULL DEFAULT '0' COMMENT 'Статус платежа', 
            pay_time int(10) NOT NULL DEFAULT '0' COMMENT 'Дата перехода к оплате', 
            success_time int(10) NOT NULL DEFAULT '0' COMMENT 'Дата оплаты', 
            amount decimal(15,2) NOT NULL DEFAULT '0' COMMENT 'Сумма платежа включая коммиссию (руб.)', 
            description text NOT NULL DEFAULT '' COMMENT 'Описание платежа', 

            PRIMARY KEY (id)  
        );";
        
        $this->execute($sql);
        echo "m170316_054931_tb_payment successfully applied.\n";
    }

    public function down()
    {
        echo "m170316_054931_tb_payment cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
