<?php

use yii\db\Migration;

class m170206_225924_tb_status extends Migration
{
    public function up()
    {
        $sql = "CREATE TABLE IF NOT EXISTS tb_status ( 
            id int(3) NOT NULL AUTO_INCREMENT, 
            name varchar(128) NOT NULL DEFAULT '', 
            PRIMARY KEY (id)
        );";

        $this->execute($sql);
        echo "m170206_225924_tb_status successfully applied.\n";
    }

    public function down()
    {
        echo "m170206_225924_tb_status cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
