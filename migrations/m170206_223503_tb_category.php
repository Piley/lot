<?php

use yii\db\Migration;

class m170206_223503_tb_category extends Migration
{
    public function up()
    {
        $sql = "CREATE TABLE IF NOT EXISTS tb_category ( 
            id int(3) NOT NULL AUTO_INCREMENT, 
            name varchar(128) NOT NULL DEFAULT '', 
            alias varchar(128) NOT NULL DEFAULT '',
            fedresurs_name varchar(256) NOT NULL DEFAULT '', 
            PRIMARY KEY (id)
        );";

        $this->execute($sql);
        echo "m170206_223503_tb_category successfully applied.\n";
    }

    public function down()
    {
        echo "m170206_223503_tb_category cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
