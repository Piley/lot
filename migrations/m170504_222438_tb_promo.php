<?php

use yii\db\Migration;

class m170504_222438_tb_promo extends Migration
{
    public function up()
    {
        $sql = "CREATE TABLE IF NOT EXISTS tb_promo ( 
            id int(10) NOT NULL AUTO_INCREMENT, 
            promo_key varchar(16) NOT NULL DEFAULT '', 
            create_time int(10) NOT NULL DEFAULT '0',
            use_time int(10) NOT NULL DEFAULT '0', 
            user_id int(11) NOT NULL DEFAULT '0', 
            channel varchar(128) NOT NULL DEFAULT '', 
            extra_data varchar(512) NOT NULL DEFAULT '', 
            UNIQUE KEY (promo_key),
            PRIMARY KEY (id) 
        );"; 

        $this->execute($sql);
        echo "m170504_222438_tb_promo successfully applied.\n";
    }

    public function down()
    {
        echo "m170504_222438_tb_promo cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
