<?php

use yii\db\Migration;

class m170213_182800_tb_lot extends Migration
{
    public function up()
    {
        $sql = "CREATE TABLE IF NOT EXISTS tb_lot ( 
            id int(10) NOT NULL AUTO_INCREMENT, 
            trade_id int(10) DEFAULT '0' COMMENT 'В каких торгах лот (tb_trade)', 
            lot_number int(10) DEFAULT '0' COMMENT 'Номер лота (федресурс)', 
            name varchar(512) NOT NULL DEFAULT '' COMMENT 'Номер торгов (федресурс)', 
            description text NOT NULL DEFAULT '' COMMENT 'Описание лота', 

            address varchar(512) NOT NULL DEFAULT '' COMMENT 'Адрес должника (федресурс)', 

            region_id int(3) NOT NULL DEFAULT '0' COMMENT 'Регион (tb_region)', 
            type_id int(3) NOT NULL DEFAULT '0' COMMENT 'Тип торгов (tb_type)', 
            status_id int(3) NOT NULL DEFAULT '0' COMMENT 'Статус торгов (tb_status)', 
            category_id int(3) NOT NULL DEFAULT '0' COMMENT 'Категория лота (tb_category)', 
            platform_id int(3) NOT NULL DEFAULT '0' COMMENT 'Площадка (tb_platform)', 

            start_price decimal(15,2) DEFAULT '0' COMMENT 'Начальная цена (руб.)', 
            now_price decimal(15,2) DEFAULT '0' COMMENT 'Текущая цена (руб.)', 
            market_price decimal(15,2) DEFAULT '0' COMMENT 'Рыночная цена (руб.)', 
            deposit_price decimal(15,2) DEFAULT '0' COMMENT 'Задаток (руб.)', 
            deposit_percent int(3) DEFAULT '0' COMMENT 'Задаток (проц.)', 
            auct_step decimal(15,2) DEFAULT '0' COMMENT 'Шаг аукциона (проц.)', 
            
            load_time int(10) DEFAULT '0' COMMENT 'Дата добавления на сайт', 
            publish_time int(10) DEFAULT '0' COMMENT 'Дата публикации (федресурс)', 
            start_time int(10) DEFAULT '0' COMMENT 'Дата начала приема заявок', 
            end_time int(10) DEFAULT '0' COMMENT 'Дата окончания приема заявок', 
            auction_time int(10) DEFAULT '0' COMMENT 'Дата проведения торгов (для аукциона)', 
            result_time int(10) DEFAULT '0' COMMENT 'Дата подведения итогов', 

            debtor_id int(10) NOT NULL DEFAULT '0' COMMENT 'Должник (tb_debtor)', 
            organizer_id int(10) NOT NULL DEFAULT '0' COMMENT 'Организатор (tb_organizer)', 
            arbitr_id int(10) NOT NULL DEFAULT '0' COMMENT 'Арбитражный управляющий (tb_arbitr)', 

            link varchar(512) DEFAULT '' COMMENT 'Ссылка (торги на площадке)', 
            fedresurs_link varchar(512) DEFAULT '' COMMENT 'Ссылка (федресурс)', 
            fedresurs_number int(10) NOT NULL DEFAULT '0' COMMENT 'Номер сообщения (федресурс)',
            case_number varchar(512) DEFAULT '' COMMENT 'Номер дела (федресурс)', 
            platform_number int(10) DEFAULT '0' COMMENT 'Номер лота на площадке (для парсеров)', 

            PRIMARY KEY (id)

            -- FOREIGN KEY (region_id) REFERENCES tb_region(id),
            -- FOREIGN KEY (type_id) REFERENCES tb_type(id),
            -- FOREIGN KEY (status_id) REFERENCES tb_status(id),
            -- FOREIGN KEY (category_id) REFERENCES tb_category(id),
            -- FOREIGN KEY (platform_id) REFERENCES tb_platform(id),
            -- FOREIGN KEY (debtor_id) REFERENCES tb_debtor(id),
            -- FOREIGN KEY (organizer_id) REFERENCES tb_organizer(id)
            
        );";
        
        $this->execute($sql);
        echo "m170213_182800_tb_lot successfully applied.\n";
    }

    public function down()
    {
        echo "m170213_182800_tb_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
