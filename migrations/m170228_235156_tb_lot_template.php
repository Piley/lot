<?php

use yii\db\Migration;

class m170228_235156_tb_lot_template extends Migration
{
    public function up()
    {
        $sql = "CREATE TABLE IF NOT EXISTS tb_lot_template ( 
            id int(10) NOT NULL AUTO_INCREMENT, 
            user_id int(11) NOT NULL DEFAULT '0',
            name varchar(128) NOT NULL DEFAULT '', 
            data text NOT NULL DEFAULT '', 
            status int(1) NOT NULL DEFAULT 0, 
            PRIMARY KEY (id) 
        );"; 

        $this->execute($sql);
        echo "m170228_235156_tb_lot_template successfully applied.\n";
    }

    public function down()
    {
        echo "m170228_235156_tb_lot_template cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
