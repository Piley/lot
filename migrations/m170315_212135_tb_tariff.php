<?php

use yii\db\Migration;

class m170315_212135_tb_tariff extends Migration
{
    public function up()
    {
        $sql = "CREATE TABLE IF NOT EXISTS tb_tariff ( 
            id int(3) NOT NULL AUTO_INCREMENT, 
            name varchar(512) NOT NULL DEFAULT '' COMMENT 'Название тарифа', 
            type varchar(512) NOT NULL DEFAULT '' COMMENT 'Тип (обычный/vip)', 
            price decimal(15,2) NOT NULL DEFAULT '0' COMMENT 'Цена (руб.)', 
            duration_time int(10) NOT NULL DEFAULT '0' COMMENT 'Время действия тарифа', 
            description text NOT NULL DEFAULT '' COMMENT 'Описание тарифа', 
            PRIMARY KEY (id)
        );";

        $this->execute($sql);
        echo "m170315_212135_tb_tariff successfully applied.\n";
    }

    public function down()
    {
        echo "m170315_212135_tb_tariff cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
