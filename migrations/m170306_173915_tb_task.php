<?php

use yii\db\Migration;

class m170306_173915_tb_task extends Migration
{
    public function up()
    {
        $sql = "CREATE TABLE IF NOT EXISTS tb_task ( 
            id int(10) NOT NULL AUTO_INCREMENT, 
            lot_id int(10) NOT NULL DEFAULT '0' COMMENT 'Лот (tb_lot)', 
            user_id int(11) NOT NULL DEFAULT '0' COMMENT 'Юзер (tb_user)', 

            name varchar(128) NOT NULL DEFAULT '' COMMENT 'Название задачи', 
            description text NOT NULL DEFAULT '' COMMENT 'Описание задачи', 

            create_time int(10) NOT NULL DEFAULT '0' COMMENT 'Дата создания', 
            deadline_time int(10) NOT NULL DEFAULT '0' COMMENT 'Дата дедлайна', 

            PRIMARY KEY (id)
        );"; 

        $this->execute($sql);
        echo "m170306_173915_tb_task successfully applied.\n";
    }

    public function down()
    {
        echo "m170306_173915_tb_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
