<?php

use yii\db\Migration;

class m170212_212207_tb_blog extends Migration
{
    public function up()
    {
        $sql = "CREATE TABLE IF NOT EXISTS tb_blog ( 
            id int(10) NOT NULL AUTO_INCREMENT, 
            name varchar(256) NOT NULL DEFAULT '' COMMENT 'Название', 
            text text NOT NULL DEFAULT '' COMMENT 'Текст', 
            category_id int(3) NOT NULL DEFAULT '0' COMMENT 'Категория (tb_blog_category)',
            user_id int(11) NOT NULL DEFAULT '0' COMMENT 'Автор (tb_user)', 
            create_time int(10) NOT NULL DEFAULT '0' COMMENT 'Дата публикации', 
            update_time int(10) NOT NULL DEFAULT '0' COMMENT 'Дата изменения', 
            type tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Тип (скрытые/открытые)', 
            keywords text NOT NULL DEFAULT '' COMMENT 'Ключевые слова', 
            description text NOT NULL DEFAULT '' COMMENT 'Описание', 
            image varchar(256) NOT NULL DEFAULT '' COMMENT 'Картинка записи', 
            alias varchar(256) NOT NULL DEFAULT '' COMMENT 'URL записи', 
            views int(10) NOT NULL DEFAULT '0' COMMENT 'Кол-во просмотров', 

            PRIMARY KEY (id) 
        );"; 

        $this->execute($sql);
        echo "m170212_212207_tb_blog successfully applied.\n";
    }

     

    public function down()
    {
        echo "m170212_212207_tb_blog cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
