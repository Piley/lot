<?php

use yii\db\Migration;

class m170226_172749_alt_user_add_role extends Migration
{
    public function up()
    {
        $sql = "ALTER TABLE tb_user ADD role int(10) NOT NULL DEFAULT '0';";
        $this->execute($sql);
        echo "m170226_172749_alt_user_add_role successfully applied.\n";
    }

    public function down()
    {
        echo "m170226_172749_alt_user_add_role cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
