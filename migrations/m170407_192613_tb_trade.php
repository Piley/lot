<?php

use yii\db\Migration;

class m170407_192613_tb_trade extends Migration
{
    public function up()
    {
        $sql = "CREATE TABLE IF NOT EXISTS tb_trade ( 
            id int(10) NOT NULL AUTO_INCREMENT, 
            type_id int(10) NOT NULL DEFAULT '0' COMMENT 'Тип',
            platform_id int(10) NOT NULL DEFAULT '0' COMMENT 'Площадка',
            organizer_id int(10) NOT NULL DEFAULT '0' COMMENT 'Организатор',
            arbitr_id int(10) NOT NULL DEFAULT '0' COMMENT 'Арб. управляющий',
            debtor_id int(10) NOT NULL DEFAULT '0' COMMENT 'Должник', 
            case_number varchar(512) NOT NULL DEFAULT '' COMMENT 'Номер дела (федресурс)', 
            link varchar(512) NOT NULL DEFAULT '' COMMENT 'Ссылка на торги', 
            number int(10) NOT NULL DEFAULT '0' COMMENT 'Номер торгов на площадке',
            number_link int(10) NOT NULL DEFAULT '0' COMMENT 'Номер в ссылке на торги на площадке',
            is_parsed tinyint(1) NOT NULL DEFAULT '0' COMMENT 'парсился = 1 не парсился = 0',
            extra_data text NOT NULL DEFAULT '' COMMENT 'Дополнительная инфа', 
            PRIMARY KEY (id)
        );";
        
        $this->execute($sql);
    }

    public function down()
    {
        echo "m170407_192613_tb_trade cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
