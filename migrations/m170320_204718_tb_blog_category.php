<?php

use yii\db\Migration;

class m170320_204718_tb_blog_category extends Migration
{
    public function up()
    {
        $sql = "CREATE TABLE IF NOT EXISTS tb_blog_category ( 
            id int(3) NOT NULL AUTO_INCREMENT, 
            name varchar(128) NOT NULL DEFAULT '', 
            alias varchar(128) NOT NULL DEFAULT '',
            PRIMARY KEY (id)
        );";

        $this->execute($sql);
        echo "m170320_204718_tb_blog_category successfully applied.\n";
    }

    public function down()
    {
        echo "m170320_204718_tb_blog_category cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
