<?php

use yii\db\Migration;

class m170212_202508_tb_organizer extends Migration
{
    public function up()
    {
        $sql = "CREATE TABLE IF NOT EXISTS tb_organizer ( 
            id int(10) NOT NULL AUTO_INCREMENT, 
            short_name varchar(512) NOT NULL DEFAULT '', 
            full_name varchar(512) NOT NULL DEFAULT '', 
            contact_person varchar(512) NOT NULL DEFAULT '', 
            region_id int(3) NOT NULL DEFAULT '0', 
            address varchar(512) NOT NULL DEFAULT '', 
            inn bigint(20) NOT NULL DEFAULT '0', 
            kpp bigint(20) NOT NULL DEFAULT '0', 
            ogrn bigint(20) NOT NULL DEFAULT '0', 
            okpo bigint(20) NOT NULL DEFAULT '0', 
            okpf varchar(128) NOT NULL DEFAULT '', 
            email varchar(512) NOT NULL DEFAULT '', 
            phone varchar(128) NOT NULL DEFAULT '', 
            type tinyint(1) NOT NULL DEFAULT '0', 
            fedresurs_link varchar(512) NOT NULL DEFAULT '', 
            PRIMARY KEY (id) 
        );"; 

        $this->execute($sql);
        echo "m170206_225924_tb_organizer successfully applied.\n";
    }

    public function down()
    {
        echo "m170212_202508_tb_organizer cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
