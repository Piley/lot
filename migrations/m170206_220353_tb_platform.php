<?php

use yii\db\Migration;

class m170206_220353_tb_platform extends Migration
{
    public function up()
    {
        $sql = "CREATE TABLE IF NOT EXISTS tb_platform ( 
            id int(3) NOT NULL AUTO_INCREMENT, 
            name varchar(512) NOT NULL DEFAULT '', 
            link varchar(512) NOT NULL DEFAULT '', 
            owner varchar(512) NOT NULL DEFAULT '', 
            description text NOT NULL DEFAULT '', 
            PRIMARY KEY (id)
        );";

        $this->execute($sql);
        echo "m170206_220353_tb_platform successfully applied.\n";
    }

    public function down()
    {
        echo "m170206_220353_tb_platform cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
