<?php

use yii\db\Migration;

class m170306_165416_tb_lot_interval extends Migration
{
    public function up()
    {
        $sql = "CREATE TABLE IF NOT EXISTS tb_lot_interval ( 
            id int(10) NOT NULL AUTO_INCREMENT, 
            lot_id int(10) NOT NULL DEFAULT '0' COMMENT 'Лот (tb_lot)', 
            number int(3) NOT NULL DEFAULT '0' COMMENT 'Номер интервала', 
            start_time int(10) NOT NULL DEFAULT '0' COMMENT 'Дата начала интервала', 
            end_time int(10) NOT NULL DEFAULT '0' COMMENT 'Дата окончания интервала', 
            price decimal(15,2) DEFAULT '0' COMMENT 'Цена на интервале (руб.)', 

            PRIMARY KEY (id)

            -- FOREIGN KEY (lot_id) REFERENCES tb_lot(id)
        );"; 

        $this->execute($sql);
        echo "m170306_165416_tb_lot_interval successfully applied.\n";
    }

    public function down()
    {
        echo "m170306_165416_tb_lot_interval cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
