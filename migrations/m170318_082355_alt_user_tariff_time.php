<?php

use yii\db\Migration;

class m170318_082355_alt_user_tariff_time extends Migration
{
    public function up()
    {
        $sql = "ALTER TABLE tb_user ADD tariff_time int(10) NOT NULL DEFAULT '0';";
        $this->execute($sql);
        echo "m170318_082355_alt_user_tariff_time successfully applied.\n";
    }

    public function down()
    {
        echo "m170318_082355_alt_user_tariff_time cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
