<?php

use yii\db\Migration;

class m170212_204422_tb_debtor extends Migration
{
    public function up()
    {
        $sql = "CREATE TABLE IF NOT EXISTS tb_debtor ( 
            id int(10) NOT NULL AUTO_INCREMENT, 
            short_name varchar(512) NOT NULL DEFAULT '', 
            full_name varchar(512) NOT NULL DEFAULT '', 
            address varchar(512) NOT NULL DEFAULT '', 
            phone varchar(128) NOT NULL DEFAULT '', 
            region_id int(3) NOT NULL DEFAULT '0', 
            category varchar(128) NOT NULL DEFAULT '', 
            inn bigint(20) NOT NULL DEFAULT '0', 
            kpp bigint(20) NOT NULL DEFAULT '0', 
            ogrn bigint(20) NOT NULL DEFAULT '0', 
            okpo bigint(20) NOT NULL DEFAULT '0', 
            okpf varchar(128) NOT NULL DEFAULT '', 

            snils bigint(20) NOT NULL DEFAULT '0', 
            birth_time int(10) NOT NULL DEFAULT '0', 
            birth_place varchar(128) NOT NULL DEFAULT '', 
            name_history varchar(512) NOT NULL DEFAULT '', 
            add_info varchar(512) NOT NULL DEFAULT '', 

            type tinyint(1) NOT NULL DEFAULT '0', 
            
            fedresurs_link varchar(512) NOT NULL DEFAULT '', 
            PRIMARY KEY (id) 
        );"; 

        $this->execute($sql);
        echo "m170206_225924_tb_debtor successfully applied.\n";
    }

    public function down()
    {
        echo "m170212_204422_tb_debtor cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
