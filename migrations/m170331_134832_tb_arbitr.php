<?php

use yii\db\Migration;

class m170331_134832_tb_arbitr extends Migration
{
    public function up()
    {
        $sql = "CREATE TABLE IF NOT EXISTS tb_arbitr ( 
            id int(10) NOT NULL AUTO_INCREMENT, 
            name varchar(512) NOT NULL DEFAULT '', 
            contact_person varchar(512) NOT NULL DEFAULT '', 
            address varchar(512) NOT NULL DEFAULT '', 
            inn bigint(20) NOT NULL DEFAULT '0', 
            email varchar(512) NOT NULL DEFAULT '', 
            phone varchar(128) NOT NULL DEFAULT '', 
            fedresurs_link varchar(512) NOT NULL DEFAULT '', 
            sro_link varchar(512) NOT NULL DEFAULT '',
            reg_number int(10) NOT NULL DEFAULT '0', 
            reg_time int(10) NOT NULL DEFAULT '0', 
            begin_time int(10) NOT NULL DEFAULT '0', 
            PRIMARY KEY (id) 
        );"; 

        $this->execute($sql);
    }

    public function down()
    {
        echo "m170331_134832_tb_arbitr cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
