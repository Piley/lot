<?php

use yii\db\Migration;

class m170502_175655_alt_user_add_notice extends Migration
{
    public function up()
    {
        $sql = "ALTER TABLE tb_user ADD notice tinyint(1) NOT NULL DEFAULT '0';";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m170502_175655_alt_user_add_notice cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
