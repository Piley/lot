<?php

use yii\db\Migration;

class m170206_222856_tb_region extends Migration
{
    public function up()
    {
        $sql = "CREATE TABLE IF NOT EXISTS tb_region ( 
            id int(3) NOT NULL AUTO_INCREMENT, 
            parent_id int(3) NOT NULL DEFAULT '0', 
            number int(3) NOT NULL DEFAULT '0', 
            name varchar(128) NOT NULL DEFAULT '', 
            genitive varchar(128) NOT NULL DEFAULT '', 
            fedresurs_name varchar(128) NOT NULL DEFAULT '', 
            PRIMARY KEY (id)
        );";

        $this->execute($sql);
        echo "m170206_222856_tb_region successfully applied.\n";
    }

    public function down()
    {
        echo "m170206_222856_tb_region cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
